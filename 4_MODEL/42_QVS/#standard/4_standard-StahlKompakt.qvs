// ******** MODEL - StahlKompakt ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-04-04	1.00		created
//
// ******** MODEL - StahlKompakt ********



// **************** **************** **************** ****************;
// *** Extract
// **************** **************** **************** ****************;

NoConcatenate
StahlKompakt_tmp:
LOAD
	*,
	Date(StahlKompakt_GueltigVon,'YYYYMMDD')							as %Key_StahlKompakt
FROM
	[$(vStdTaskQvdTransactKc)\StahlKompakt.StaffelPreis.qvd]
	(qvd)
;



// **************** **************** **************** ****************;
// *** Kalender
// **************** **************** **************** ****************;

Left Join
(StahlKompakt_tmp)
LOAD Distinct
	Key_Kalender_JahrMonatNr,
	Key_Kalender_Geschaeftsjahr,
	Kalender_GJ_Von,
	Kalender_GJ_Bis
FROM
	[$(vStdTaskQvdTransact)\KC.Kalender.qvd]
	(qvd)
;



// **************** **************** **************** ****************;
// *** Fakt
// **************** **************** **************** ****************;

Concatenate
(Fakt_tmp)
LOAD Distinct

	// Key
	%Key_StahlKompakt,

	// Kalender
	Kalender_GJ_Von,
	Kalender_GJ_Bis,

	// Fakt
//	Key_Kalender_Geschaeftsjahr											as Fakt_Geschaeftsjahr,
//	Key_Kalender_JahrMonatNr											as Fakt_JahrMonatNr,
	Date(StahlKompakt_GueltigVon)										as Fakt_DatumVon,
	Date(StahlKompakt_GueltigBis)										as Fakt_DatumBis,
	'S'																	as Fakt_Typ,
	'StahlKompakt'														as Fakt_TypAlias

Resident
	StahlKompakt_tmp
;



// **************** **************** **************** ****************;
// *** StahlKompakt
// **************** **************** **************** ****************;

NoConcatenate
StahlKompakt:
LOAD Distinct

	// Keys
	%Key_StahlKompakt,
	Key_StahlKompakt,

	// StahlKompakt
	StahlKompakt_Id,
	StahlKompakt_GueltigJahrMonatNr,
	// Material
	StahlKompakt_MaterialBezeichnung,
	StahlKompakt_MaterialBezeichnungVoll,
	StahlKompakt_MaterialLegierung,
	StahlKompakt_MaterialName,
	StahlKompakt_MaterialNr,
	StahlKompakt_MaterialFormat,
	StahlKompakt_MaterialGeometrie,
	StahlKompakt_MaterialGeometrieVoll,
	StahlKompakt_MaterialDurchmesser,
	StahlKompakt_MaterialDurchmesserOrder,
	StahlKompakt_MaterialBeschaffenheit,
	// Menge
	StahlKompakt_StaffelMenge,
	StahlKompakt_StaffelMengeVon,
	StahlKompakt_StaffelMengeBis,
	// Preis
	StahlKompakt_StaffelPreisProTonne,
	StahlKompakt_StaffelPreisProTonneVormonat,
	StahlKompakt_StaffelPreisProTonneVorjahr,
	// Vergleich
	StahlKompakt_StaffelPreisVergleichVormonat,
	StahlKompakt_StaffelPreisVergleichVorjahr

Resident
	StahlKompakt_tmp
;



// **************** **************** **************** ****************;
// *** Waehrung
// **************** **************** **************** ****************;

FOR Each vStahlKompaktWaehrung in $(vCalcWaehrungRate)

	Left Join
	(StahlKompakt)
	LOAD Distinct
		Key_StahlKompakt,
		[StahlKompakt_StaffelPreisProTonne_$(vStahlKompaktWaehrung)]
	Resident
		StahlKompakt_tmp
	;

NEXT vStahlKompaktWaehrung



// **************** **************** **************** ****************;
// *** Clear
// **************** **************** **************** ****************;

CALL subCommonDropTable('StahlKompakt_tmp');
DROP Field Key_StahlKompakt;
LET vStahlKompaktWaehrung		= Null();
