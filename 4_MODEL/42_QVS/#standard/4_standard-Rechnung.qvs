// ******** MODEL - Rechnung ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-04-06	1.00		created
// 2018-07-05	1.01		modified
// 2018-07-19	1.02		add Field: Rechnung_Text
//
// ******** MODEL - Rechnung ********



// **************** **************** **************** ****************;
// *** Rechnung
// **************** **************** **************** ****************;

NoConcatenate
Fakt_Rechnung_tmp:
LOAD
	*
FROM
	[$(vStdTaskQvdTransact)\Einkaufstracker.RechnungKc.qvd]
	(qvd)
;



// **************** **************** **************** ****************;
// *** Fakt
// **************** **************** **************** ****************;


NoConcatenate
Fakt_Rechnung_pre:
LOAD

	// Keys
	Key_Rechnung,
	Key_Iso4217_Alpha,
	Key_WerkGesellschaft_Artikel_Nr,
	Key_WerkGesellschaft_Artikel,
	Key_Artikel_Nr,
	Key_Artikel,
	Key_WerkGesellschaft_Warengruppe,
	Key_WerkGesellschaft_Lager_Warengruppe,
	Key_Warengruppe,
	Key_WerkGesellschaft_Lieferant_Nr							as Key_WerkGesellschaft_KreditorLieferant_Nr,
	Key_WerkGesellschaft_Lieferant								as Key_WerkGesellschaft_KreditorLieferant,
	Key_Lieferant_Nr											as Key_KreditorLieferant_Nr,
	Key_Lieferant												as Key_KreditorLieferant,
	Key_Anschrift,
	Key_WerkGesellschaft,
	Key_WerkGesellschaft_Lager,
	Key_Wechselkurs,

	// Fakt
	Key_Kalender_Geschaeftsjahr									as Fakt_Geschaeftsjahr,
	Date(Key_Kalender_Datum)									as Fakt_DatumVon,
	Date(Key_Kalender_Datum)									as Fakt_DatumBis,
	'R'															as Fakt_Typ,
	'Rechnung'													as Fakt_TypAlias,
	Rechnung_MengeEinheit										as Fakt_MengenEinheit,
	Rechnung_PreisEinheit										as Fakt_PreisEinheit,
	Rechnung_Abc_Artikel										as Fakt_Abc_Artikel,
	Rechnung_Abc_Lieferant										as Fakt_Abc_Lieferant,
	Rechnung_Abc_Lieferant										as Fakt_Abc_KreditorLieferant,

	// Flag
	Flag_Scope,
	Flag_ScopeAlias,
	Flag_GeschaeftsjahrAbgeschlossen,
	0															as Flag_Geschaeftsjahr,
	Flag_WerkGesellschaft,
	Flag_Artikel,
	Flag_Lieferant,

	// Kalender
	Kalender_GJ_Von,
	Kalender_GJ_Bis,

	// Kategorie
	Kategorie_Wert_Von,
	Kategorie_Wert_Bis,
	Kategorie_Wert_Name,
	Kategorie_Wert_NameEur,
	Kategorie_Wert_NamePositiv,
	Kategorie_Wert_NameEurPositiv,

	// Bestellung
	Bestellung_Nr,
	Bestellung_Position,

	// Rechnung
	Rechnung_Nr,
	Rechnung_Position,
	Date(Key_Kalender_Datum)									as Rechnung_Datum,
	Rechnung_Waehrung,
	Rechnung_Id,
	Rechnung_NrExtern,
	Rechnung_DatumEingang,
	Rechnung_DatumZahlung,
	Rechnung_GeschaeftsjahrNum,
	Rechnung_Text,
	Rechnung_Menge,
	Rechnung_MengeEinheit,
	Rechnung_PreisEinheit,
	Rechnung_Preis,
	Rechnung_PreisRabatt,
	Rechnung_Rabatt,
	Rechnung_Wert,
	Rechnung_WertBetrag,
	Rechnung_Beschaffungsnebenkosten,
	Rechnung_SachkontoNr,
	Rechnung_SachkontoBezeichnung,
	Rechnung_KostenstelleNr,
	Rechnung_KostenstelleBezeichnung,
	Rechnung_Zahlungskondition,

	// Abc
	Rechnung_Abc_Artikel,
	Rechnung_Abc_ArtikelWarengruppe1,
	Rechnung_Abc_ArtikelWarengruppe2,
	Rechnung_Abc_ArtikelWarengruppe3,
	Rechnung_Abc_ArtikelWarengruppe1_KC,
	Rechnung_Abc_ArtikelWarengruppe2_KC,
	Rechnung_Abc_ArtikelWarengruppe3_KC,
	Rechnung_Abc_Lieferant,
	Rechnung_Abc_LieferantWarengruppe1,
	Rechnung_Abc_LieferantWarengruppe2,
	Rechnung_Abc_LieferantWarengruppe3,
	Rechnung_Abc_LieferantWarengruppe1_KC,
	Rechnung_Abc_LieferantWarengruppe2_KC,
	Rechnung_Abc_LieferantWarengruppe3_KC

Resident
	Fakt_Rechnung_tmp
;



// **************** **************** **************** ****************;
// *** Wechselkurs
// **************** **************** **************** ****************;

FOR Each vRechnungWaehrung in $(vCalcWaehrungRate)

	Left Join
	(Fakt_Rechnung_pre)
	LOAD
		Key_Rechnung,
		[Rechnung_Preis_$(vRechnungWaehrung)],
		[Rechnung_Preis_PreisEinheit_$(vRechnungWaehrung)],
		[Rechnung_PreisRabatt_$(vRechnungWaehrung)],
		[Rechnung_PreisRabatt_PreisEinheit_$(vRechnungWaehrung)],
		[Rechnung_Rabatt_$(vRechnungWaehrung)],
		[Rechnung_Wert_$(vRechnungWaehrung)]
	Resident
		Fakt_Rechnung_tmp
	;

NEXT vRechnungWaehrung



// **************** **************** **************** ****************;
// *** Final
// **************** **************** **************** ****************;

Concatenate
(Fakt_tmp)
LOAD
	*
Resident
	Fakt_Rechnung_pre
;


// Clear
CALL subCommonDropTable('Fakt_Rechnung_tmp');
CALL subCommonDropTable('Fakt_Rechnung_pre');
LET vRechnungWaehrung		= Null();
