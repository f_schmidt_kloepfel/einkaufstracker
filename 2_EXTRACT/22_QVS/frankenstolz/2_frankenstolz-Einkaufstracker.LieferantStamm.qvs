// ******** EXTRACT - Einkaufstracker.KreditorStamm ********
// 
// author:		Felix Schmidt
// 
//
// LOG:
// modified		version		description
// 2017-06-12	1.00		created
// 2017-09-20	2.00		mod: Template.xlsx
// 2017-11-16	2.10		add Field: KreditorenID
// 2017-11-17	3.00		rename: Kreditor to KreditorStamm
// 
// ******** EXTRACT - Einkaufstracker.KreditorStamm ********



// **************** **************** **************** ****************
// ***  KreditorStamm
// **************** **************** **************** ****************

//NoConcatenate
Einkaufstracker.KreditorStamm:
LOAD
	'Frankenstolz' 					as Gesellschaft,
	Text(KreditorenNr)				as KreditorenNr,
	KreditorenUStID,
	KreditorenName,
	Adresse,
	Land,
	Ort,
	PLZ,
	Ansprechpartner_Anrede,
	Ansprechpartner_Vorname,
	Ansprechpartner_Nachname,
	Email,
	Homepage,
	Telefon,
	Mobil,
	InterCompany,
	DUNSNr,
	Zahlungsbedingungen as Zahlungsbedingungen_CODE,///CODE -> Zahlungsbedingungen
	//Nullwerte
	null()							as WerkStandort,
	null()							as WerkNr,
	null()							as WerkName,
	Text(null())					as KreditorenID
	
	
	
FROM
[$(vStdTaskDiv)\*Datenabfrage Projektvorbereitung*.xlsx]
(ooxml, embedded labels, table is Kreditorenstammdaten)
;

////ZB_Mapping
left join (Einkaufstracker.KreditorStamm)
LOAD Code 									as Zahlungsbedingungen_CODE, 
	  Beschreibung							as Zahlungsbedingungen,
     trim(replace(Faelligkeitsformel,'T',''))						
											as NettoTage, 
     trim(replace(Skontoformel,'T',''))
											as SkontoTage, 
     [Skonto %]								as SkontoSatz 
/*      [Skonto auf Gutschrift berech.], 
     [Skontoformel 2], 
     [Skonto 2 in %], 
     [Skonto 2 auf Gutschrift berech.], 
     [Skontoformel 3], 
     [Skonto 3 in %], 
     [Skonto 3 auf Gutschrift berech.], 
     Valutadatumsformel, 
     Dekadenzahlung, 
     Provisionskennzeichen */
FROM
[$(vStdTaskDiv)\*Zahlungsbedingungen.xlsx]
(ooxml, embedded labels, header is 1 lines, table is Zahlungsbedingungen1);

/////Incoterms
left join (Einkaufstracker.KreditorStamm)
LOAD distinct
	 Text(Nr.)						as KreditorenNr,
	 Lieferbedingungscode 			as Incoterm	 
	/*  [Skonto (MW)] 					as skonto_2016//Skonto 
     Name, 
     Zuständigkeitseinheitencode, 
     Lagerortcode, 
     [PLZ-Code], 
     [Länder-/Regionscode], 
     Ort, 
     Telefonnr., 
     Kontakt, 
     Einkäufercode, 
     [Zlg.-Bedingungscode], 
     Suchbegriff, 
     Adresse, 
     [Adresse 2], 
     [Unsere Kontonr.], 

     Rechnungsrabattcode, 
     Zahlungsformcode, 

     [Zinsrechnungsbetrag (MW)], 
     [USt-IdNr.], 
     Sollbetrag, 
     [E-Mail], 
     [Primäre Kontaktnr.] */
FROM
[$(vStdTaskDiv)\KreditorenWerkeEinkaeufer.xlsx]
(ooxml, embedded labels, table is [2017])
where len(trim([Nr.]))>1 and len(trim(Lieferbedingungscode))>1;


Rename Field KreditorenNr to LieferantenNr;
Rename Field KreditorenID to LieferantenID;
Rename Field KreditorenUStID to LieferantenUStID;
Rename Field KreditorenName to LieferantenName;
Rename Table Einkaufstracker.KreditorStamm to Einkaufstracker.LieferantStamm;



