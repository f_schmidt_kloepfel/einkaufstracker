// ******** EXTRACT - Einkaufstracker.Bestellung ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-01-09	1.00		created
// 2018-01-20	2.00		modified
// 2018-02-06	2.01		add: SimpleSystem
// 2018-02-09	2.02		mod: Kontierungstyp
// 2018-02-15	2.10		modified
// 2018-02-16	2.20		mod: E_Tracker
// 2018-02-16	2.21		add: SUL
// 2018-02-17	2.30		mod Field: Bestelldatum
// 2018-04-04	2.40		mod Mapping: Map.Einkaufstracker.Bestellung_Monat
// 2018-04-10	2.50		mod: Final
// 2018-04-20	2.51		mod E_Tracker: Where, Bestelldatum
// 2018-05-04	2.60		add: BANF
// 2018-05-22	2.61		mod Field: Bestelldatum
// 2018-06-19	2.62		add: KAT
// 2018-06-22	2.63		mod: SAP, E_Tracker
// 2018-06-23	2.70		mod: E_Tracker
// 2018-06-24	2.71		mod: SAP (Lieferdatum)
// 2018-06-25	2.72		mod: E_Tracker
// 2018-07-19	2.80		add: PCN
//
// ******** EXTRACT - Einkaufstracker.Bestellung ********


TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: Prepare;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.Bestellung_tmp:
LOAD * Inline [
	FILE_NAME
];


Map.Einkaufstracker.Bestellung_Monat:
Mapping
LOAD Distinct
	Calendar_MonthName			as IN,
	Calendar_MonthNumber		as OUT
FROM
	[$(vStdTaskQvdFilesMap)\Calendar.MonthNo.qvd]
	(qvd)
;


Map.Einkaufstracker.Bestellung_SimpleSystem:
Mapping
LOAD * Inline [
	IN,												OUT
	'Conrad Electronic SE',							'9050350'
	'Hoffmann GmbH',								'9151300'
	'Fleischhauer & Rudroff',						'9610394'
	'Kaiser + Kraft GmbH',							'9211100'
	'Ashton-Feucht GmbH',							'9696410'
	'Kroschke sign-international GmbH',				'9213980'
	'Mayersche Buchhandlung',						'102329'
	'VWR International GmbH',						'9651826'
];
//	'Igefa Handelsgesellschaft mbH & Co. KG',		''



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: KWE - SE;
TRACE **************** **************** **************** ****************;


// **************** ****************
// *** SAP
// **************** ****************

Map.Einkaufstracker.Bestellung_Kontierung_SAP:
Mapping
LOAD Distinct
	K								as IN,
	[Bez. Kontierungstyp]			as OUT
FROM
	[$(vStdTaskDiv)\Bestellung\Kontierungszuordnung.txt]
	(txt, utf8, embedded labels, delimiter is '\t', msq, header is 1 lines)
Where
		Len(Trim(K))=1
;

Map.Einkaufstracker.Bestellung_Lieferdatum:
Mapping
LOAD Distinct
	Num(Einkaufsbeleg) &'#'& Num(Position)					as IN,
	Date(Min(Lieferdatum))									as OUT
//	Einkaufsbeleg,
//	[Lieferant/Lieferwerk],
//	Position,
//	Einteilung,
//	Einkaufsbelegart,
//	Einkaufsbelegtyp,
//	Einkäufergruppe,
//	[Bestellentwicklung / Abrufdoku],
//	Belegdatum,
//	Material,
//	Kurztext,
//	Warengruppe,
//	Löschkennzeichen,
//	Positionstyp,
//	Kontierungstyp,
//	Werk,
//	Lagerort,
//	Bestellmenge,
//	Bestellmengeneinheit,
//	Nettopreis,
//	Währung,
//	Preiseinheit,
//	Einteilungsmenge,
//	Lieferdatum,
//	Uhrzeit,
//	Stat.Lieferdatum,
//	[Vorige Menge],
//	[Eingegangene Menge],
//	[Ausgegebene Menge],
//	[Gelieferte Menge],
//	Bestellanforderung,
//	[Banf-Position],
//	Erstellungskennz.,
//	[Anzahl der Positionen]
FROM
	[$(vStdTaskDiv)\Bestellung\Einteilungen SAP *.XLSX]
	(ooxml, embedded labels, table is Sheet1)
Where
		Belegdatum<=Lieferdatum
Group By
	Einkaufsbeleg
	,Position
;


Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FileName()												as FILE_NAME,
	Num(Einkaufsbeleg) &'#'& Num(Position)					as sap_Key_Bestellung,

	'KWE'													as WerkNr,
	Text(Werk)												as LagerNr,
	Text(Material)											as tmp_ArtikelNr,
	Text(Warengruppe)										as Warengruppe_Code,
	Text(SubField([Lieferant/Lieferwerk],' ',1))			as LieferantenNr,
	Trim(Replace([Lieferant/Lieferwerk]
		,SubField([Lieferant/Lieferwerk],' ',1),''))		as LieferantenName,
	Einkäufergruppe											as tmp_UserID_Besteller,
	Einkäufergruppe											as StrategischerEinkaeufer,

	Einkaufsbeleg											as BestellNr,
	Position												as BestellPosition,
	Einkaufsbelegart,
	Einkaufsbelegtyp,
//	[Bestellentwicklung / Abrufdoku],
	If(IsNum(Belegdatum),Belegdatum
		,Date(Floor(MakeDate(2017,7))))						as Bestelldatum,
	ApplyMap('Map.Einkaufstracker.Bestellung_Lieferdatum',Num(Einkaufsbeleg) &'#'& Num(Position)
		,Null())											as Liefertermin_laut_Bestellung,
	Löschkennzeichen,
	Positionstyp,
	If(Len(Trim(Kontierungstyp))>0,Kontierungstyp)			as Kontierungstyp,
	ApplyMap('Map.Einkaufstracker.Bestellung_Kontierung_SAP'
		,Kontierungstyp,'Serie')							as KontierungstypAlias,
//	Lagerort,
	Bestellmenge,
	Bestellmengeneinheit									as Mengeneinheit,
//	[Menge in Lager-ME]										as LagerMengeBestand,
//	Lagermengeneinheit										as LagerMengeneinheit,
	Nettopreis												as Bestellpreis,
	Trim(Währung)											as tmp_Waehrung,
	Preiseinheit,
//	Zielmenge,
	Bestellnettowert										as Bestellwert//,
//	[Offene Zielmenge],
//	[noch zu liefern (Menge)],
//	[noch zu liefern (Wert)],
//	[noch zu berechnen (Menge)],
//	[noch zu berechnen (Wert)],
//	[Anzahl der Positionen]
FROM
	[$(vStdTaskDiv)\Bestellung\Bestellungen SAP *.XLSX]
	(ooxml, embedded labels, table is Sheet1)
Where
		Len(Trim(Einkaufsbeleg))>0
;



// **************** ****************
// *** INFOR
// **************** ****************

Map.Einkaufstracker.Bestellung_infor:
Mapping
LOAD Distinct
	Text(Bestell) &'#'& Text(Pos)							as IN,
	[Best.dat]												as OUT
FROM
	[$(vStdTaskDiv)\Bestellung\INFOR\Bestellungen INFOR 2017 KWE *.xlsx]
	(ooxml, embedded labels, table is winClient)
;


//NoConcatenate
Einkaufstracker.Bestellung_infor:
LOAD
	FileName()												as FILE_NAME,
	Num(Bestell) &'#'& Num(Pos)								as infor_Key_Bestellung,

	'KWE'													as WerkNr,
	Text(Teilenummer)										as tmp_ArtikelNr,
	E														as Flag_CPD_Artikel,
	Text(Lieferant)											as LieferantenNr,
	Name													as LieferantenName,
	Einkäuf													as tmp_UserID_Besteller,
	Ek.Grpp													as StrategischerEinkaeufer,

	Bestell													as BestellNr,
	Pos														as BestellPosition,
//	[offene Menge],
	BME														as Mengeneinheit,
	[Obligo Bestell],
	St,
	Bestellmenge,
	Bestellpreis,
	Bestellwert,
	Trim(Wrg)												as tmp_Waehrung,
	[Teile-Bez]												as Bestelltext,
	Typ,
	Teilekommiss,
	Al,
	Herstel,
	Zulieferer,
	Wk,
	LO,
	ApplyMap('Map.Einkaufstracker.Bestellung_infor',Text(Bestell) &'#'& Text(Pos)
		,Date(Floor(MakeDate(SubField(FileName(),' ',3)))))	as Bestelldatum,
	Liefert													as Liefertermin_laut_Bestellung,
	[AB-Datum]												as Liefertermin_laut_Auftragsbestaetigung,
	Adresse,
	Plz														as PLZ,
	Ort,
	Kommission,
	Text(Vertr)												as KontraktNr,
	Text(Vertragposition)									as KontraktPosition,
	Vertragstyp,
//	[AB-Datum],
	Verursacher,
	Terminposition,
	Auftragsnr.Lief											as Externe_BestellNr
FROM
	[$(vStdTaskDiv)\Bestellung\Bestellungen INFOR *.xlsx]
	(ooxml, embedded labels, table is winClient)
Where
		Len(Trim(Bestell))>0
;


Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	*
Resident
	Einkaufstracker.Bestellung_infor
Where
		not Exists(sap_Key_Bestellung,infor_Key_Bestellung)
;


// Clear
CALL subCommonDropTable('Einkaufstracker.Bestellung_infor');



// **************** ****************
// *** SimpleSystem
// **************** ****************

Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FILE_NAME,
	WerkNr,
	Warengruppe_Code,
	If(IsNum(LieferantenNr),Text(LieferantenNr)
		,LieferantenNr)										as LieferantenNr,
	If(not IsNum(LieferantenNr),LieferantenNr)				as LieferantenName,
	tmp_UserID_Besteller,
	Bestelldatum,
	BestellNr,
	Warengruppe_Code										as Bestelltext,
	tmp_Waehrung,
	Bestellwert
;
LOAD
	'SimpleSystem_'& FileName()								as FILE_NAME,
	'KWE'													as WerkNr,
	'SimpleSystem'											as Warengruppe_Code,
	ApplyMap('Map.Einkaufstracker.Bestellung_SimpleSystem',Lieferant
		,Lieferant)											as LieferantenNr,
	Benutzer												as tmp_UserID_Besteller,
	Date(Bestelldatum)										as Bestelldatum,
	Text(Bestellnummer)										as BestellNr,
	'EUR'													as tmp_Waehrung,
	Num(Auftragswert)										as Bestellwert
FROM
	[$(vStdTaskDiv)\Bestellung\SimpleSystem\Bestellungen_Umsätze.csv]
	(txt, codepage is 1252, embedded labels, delimiter is ';', msq)
;



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: E_Tracker;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.Bestellung_ETracker_tmp:
LOAD * Inline [
	FILE_WERK
];

Concatenate
(Einkaufstracker.Bestellung_ETracker_tmp)
LOAD
	*,
	FileName()												as FILE_NAME,
	RecNo()													as FILE_RECNO,
	Num(Left(FileName(),8))									as FILE_DATE,
	Left(SubField(FileName(),'-',-1),3)						as FILE_WERK
FROM
	[$(vStdTaskDiv)\E_Tracker\*-E_Tracker-*.xlsx]
	(ooxml, embedded labels, table is [Bestelldaten | Order Data])
;


NoConcatenate
Einkaufstracker.Bestellung_ETracker_pre:
LOAD
	*,
	FILE_WERK
		&'#'& Text(BestellNr)
		&'#'& Text(BestellPosition)							as Key_Werk_Bestellung_Nr_Position
Resident
	Einkaufstracker.Bestellung_ETracker_tmp
Where
		not Match(BestellNr,'Order No','EBELN','ORDER_NO')
;

Left Join
(Einkaufstracker.Bestellung_ETracker_pre)
LOAD
	Key_Werk_Bestellung_Nr_Position,
	Max(FILE_DATE)											as FILE_DATE,
	1														as FLAG_CURRENT
Resident
	Einkaufstracker.Bestellung_ETracker_pre
Where
		Len(Trim(BestellNr))>0
Group By
	Key_Werk_Bestellung_Nr_Position
;


Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FILE_NAME,
	FILE_RECNO,
	FILE_DATE,
	FILE_WERK										as WerkNr,
	Text(ArtikelNr)									as tmp_ArtikelNr,
	Text(Warengruppe_Code)							as Warengruppe_Code,
	Text(LieferantenNr)								as LieferantenNr,
	Text(UserID_Besteller)							as tmp_UserID_Besteller,

	Text(BestellNr)									as BestellNr,
	Text(BestellPosition)							as BestellPosition,
	Bestelltext,
	Bestellmenge,
	Mengeneinheit,
	Bestellpreis,
	Rabatt,
	Bestellpreis_Abzüglich_Rabatt					as Bestellpreis_Abzueglich_Rabatt,
	Preiseinheit,
	Bestellwert,
	If(FILE_WERK='PCZ' and Währung='_x0002_','CZK'
		,If(Upper(Währung)='EURO','EUR'
		,If(Upper(Währung)='DOLLAR','USD'
		,Trim(Währung))))							as tmp_Waehrung,
	If(FILE_NAME='20180711-E_Tracker-KNA_201805.xlsx',Date(Floor(MonthEnd(MakeDate(2018,05))))
		,If(Match(FILE_WERK,'KIT','SIR'),Date(Floor(MakeDate(Right(Bestelldatum,4),Mid(Bestelldatum,3,2),Left(Bestelldatum,2))))
		,If(FILE_WERK='PRO' and Index(Bestelldatum,'/'),Date(Floor(MakeDate(Left(SubField(Bestelldatum,'/',3),4),SubField(Bestelldatum,'/',1),SubField(Bestelldatum,'/',2))))
		,If(FILE_WERK='KRO',If(IsNum(Bestelldatum),Bestelldatum,Date(Floor(MakeDate(Left(SubField(FileName(),'_',-1),4),Mid(SubField(FileName(),'_',-1),5,2)))))
		,If(Index(Bestelldatum,'-') and IsNum(SubField(SubField(Bestelldatum,' ',1),'-',2))
			,Date(Floor(MakeDate(SubField(SubField(Bestelldatum,' ',1),'-',1),SubField(SubField(Bestelldatum,' ',1),'-',2),SubField(SubField(Bestelldatum,' ',1),'-',3))))
		,If(Index(Bestelldatum,'-')
			,Date(Floor(MakeDate(2000+SubField(SubField(Bestelldatum,' ',1),'-',3),ApplyMap('Map.Einkaufstracker.Bestellung_Monat',SubField(SubField(Bestelldatum,' ',1),'-',2)),SubField(SubField(Bestelldatum,' ',1),'-',1))))
		,Date(Bestelldatum)))))))					as Bestelldatum,
	Text(Bestelldatum)								as BestelldatumOriginal,
	Liefertermin_laut_Bestellung,
	Liefertermin_laut_Auftragsbestätigung			as Liefertermin_laut_Auftragsbestaetigung,
	If(Len(Trim(Liefertermin_laut_Bestellung))>0 or Len(Trim(Liefertermin_laut_Auftragsbestätigung))>0
		,1,0)										as Flag_Liefertermin,
	KontraktNr,
	RechnungsNr,
	RechnungsPosition
//	BestellStatus,
//	BestellKennzeichen

Resident
	Einkaufstracker.Bestellung_ETracker_pre
Where
		(Len(Trim(BestellNr))=0 or FLAG_CURRENT=1)
;


// Clear
CALL subCommonDropTable('Einkaufstracker.Bestellung_ETracker_tmp');
CALL subCommonDropTable('Einkaufstracker.Bestellung_ETracker_pre');



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: Katek;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.Bestellung_katek:
LOAD
	FileName()												as FILE_NAME,
	EKBA,
	If(Lower(Trim(Material))<>'ohne nummer',Text(Material))	as tmp_ArtikelNr,
	Kurztext												as Bestelltext,
	EKG,
	Datum													as Bestelldatum,
	Text(SubField([Lieferant/Lieferwerk],' ',1))			as LieferantenNr,
	Trim(Replace([Lieferant/Lieferwerk]
		,SubField([Lieferant/Lieferwerk],' ',1),''))		as LieferantenName,
	Einkaufsbeleg											as BestellNr,
	1														as BestellPosition,
	WGR														as Warengruppe_Code,
	Text(Werk)												as LagerNr,
	If(Lower(Trim(Werk1))='katek gmbh','SGR'
		,If(Lower(Trim(Werk1))='katek hungary','SHU'))		as WerkNr,
	Bestellt												as Bestellmenge,
	ME														as Mengeneinheit,
	Preis													as Bestellpreis,
	Trim(Replace(Whg,'EURO','EUR'))							as tmp_Waehrung,
	PE														as Preiseinheit,
	[Wert Whg]												as Bestellwert,
	Whg1,
	[Wert Euro]
FROM
	[$(vStdTaskDiv)\Bestellung\Bestellungen Non-BOM ZTX 2017 für KWE 17012018.XLSX]
	(ooxml, embedded labels, table is Bestellungen)
;


NoConcatenate
Einkaufstracker.Bestellung_katek_order:
LOAD
	FILE_NAME,
	tmp_ArtikelNr,
	Bestelltext,
	Bestelldatum,
	LieferantenNr,
	LieferantenName,
	BestellNr,
	If(Peek(BestellNr)=BestellNr,Peek(BestellPosition)+1
		,BestellPosition)									as BestellPosition,
	Warengruppe_Code,
	LagerNr,
	WerkNr,
	Bestellmenge,
	Mengeneinheit,
	Bestellpreis,
	tmp_Waehrung,
	Preiseinheit,
	Bestellwert
Resident
	Einkaufstracker.Bestellung_katek
Order By
	BestellNr
;


Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD Distinct
	*
Resident
	Einkaufstracker.Bestellung_katek_order
;


// Clear
CALL subCommonDropTable('Einkaufstracker.Bestellung_katek');
CALL subCommonDropTable('Einkaufstracker.Bestellung_katek_order');



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: PAT;
TRACE **************** **************** **************** ****************;

Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FileName()												as FILE_NAME,

	'PAT'													as WerkNr,
	Text(Teil)												as tmp_ArtikelNr,
	Bezeichnung1											as ArtikelName,
	Bezeichnung2											as ArtikelName2,
	Bezeichnung3											as ArtikelName3,
	If(Len(Trim(Replace(Verwendung,'?','')))>0
		,Replace(Verwendung,'fil','FIL'))					as Warengruppe_Code,
	Text([supplier code PAT])								as LieferantenNr,
	[supplier name]											as LieferantenName,
	[ABC-Wert]												as Bestellwert,
	Trim(Wä)												as tmp_Waehrung,
	Date(Floor(MonthEnd(MakeDate(SubField(FileName(),'_',4)
		,SubField(FileName(),'_',5)))))						as Bestelldatum,
	Klasse,
	MengeAuftrag,
	MengeUmsatz,
	MengeRohertrag,
//	MengeBestellung,
	MengeRK													as Bestellmenge,
	ME														as Mengeneinheit,
//	Kontrolle,
	[Preis/Stk]												as Bestellpreis,
	Incoterm,
	If(Len(Trim([supplier code 
KWE]))>1,Text([supplier code 
KWE]))														as LieferantenNr_KWE

FROM
	[$(vStdTaskDiv)\Bestellung\PAT\EK_Datenexport_Einkaufsdaten_*_Mandant_1_ABC_Teile.xlsx]
	(ooxml, embedded labels, table is Tabelle1)
;



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: KAT;
TRACE **************** **************** **************** ****************;

Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FileName()												as FILE_NAME,

	'KAT'													as WerkNr,
	Text([WHG-Nr])											as Warengruppe_Code,
	Text([Art-Nr])											as tmp_ArtikelNr,
	Artikelbezeichnung										as ArtikelName,
	Lieferant												as LieferantenName,
	Date([WE-Datum])										as Bestelldatum,
	Menge													as Bestellmenge,
	Einheit													as Preiseinheit,
	Währung													as tmp_Waehrung,
	[EK-Preis]												as Bestellpreis,
	[rab. EK-Wert]											as Bestellwert

FROM
	[$(vStdTaskDiv)\Bestellung\20180604-KAT-Wareneingänge Drittlieferanten_2017.xls]
	(biff, embedded labels, table is [Wareneingänge Drittlieferanten_$])
;



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: SUL;
TRACE **************** **************** **************** ****************;

Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FileName()												as FILE_NAME,

	'SUL'													as WerkNr,
	Text(Kostenart)											as Warengruppe_Code,
	Text([AB Nr.])											as tmp_ArtikelNr,
	Bestellartikel											as ArtikelName,
	Lieferant												as LieferantenNr,
	Lieferant												as LieferantenName,
	Text(Besteller)											as tmp_UserID_Besteller,
	Text(Bestellnummer)										as BestellNr,
	Date(Bestelldatum)										as Bestelldatum,
	Date(Datum)												as Liefertermin_laut_Bestellung,
	Bestellartikel											as Bestelltext,
	'EUR'													as tmp_Waehrung,
	[Rechnung
2017
Bruttowert €]												as Bestellwert,
	[Gutschrift Nr.
Rechnung Nr.]												as RechnungsNr

FROM
	[$(vStdTaskDiv)\Bestellung\180117-E_Tracker-SUL.xls]
	(biff, embedded labels, header is 1 lines, table is [Bestellungen 2017$])
Where
		Lieferant<>'Total' and Len(Trim(Lieferant))>0
;



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: PCN;
TRACE **************** **************** **************** ****************;

Map.Einkaufstracker.Bestellung_PCN_MengenEinheit:
Mapping
LOAD Distinct
	Text(Einheit)					as IN,
	Bezeichnung						as OUT
FROM
	[$(vStdTaskDiv)\????????-MengenEinheit_Definition.csv]
	(txt, utf8, embedded labels, delimiter is ';', msq)
;



Concatenate
(Einkaufstracker.Bestellung_tmp)
LOAD
	FileName()												as FILE_NAME,

	// Keys
	'PCN'													as WerkNr,
	Text(K)													as tmp_ArtikelNr,
	L														as ArtikelName,
	Text(C)													as LieferantenNr,
	D														as LieferantenName,

	// Bestellung
	Text(A)													as BestellNr,
	Date(MakeDate(SubField(B,'-',1)
		,SubField(B,'-',2)
		,SubField(B,'-',3)))								as Bestelldatum,
	Date(MakeDate(SubField(Q,'-',1)
		,SubField(Q,'-',2)
		,SubField(Q,'-',3)))								as Liefertermin_laut_Bestellung,
	G														as Bestelltext,

	// Menge
	Num(M)													as Bestellmenge,
	ApplyMap('Map.Einkaufstracker.Bestellung_PCN_MengenEinheit',Text(N)
		,N)													as Mengeneinheit,

	// Preis/Wert
	E														as tmp_Waehrung,
	Num(O)													as Bestellpreis,
	Num(O)													as Bestellpreis_Abzueglich_Rabatt,
	Num(T)													as Bestellwert

//	F,
//	H,
//	I,
//	J,
//	P,
//	R,
//	S,
//	U,
//	V

FROM
	[$(vStdTaskDiv)\Original\PCN\????????-PCN_ERP-*.xlsx]
	(ooxml, no labels, header is 1 lines, table is Order)
Where
		Len(Trim(E))=3
;



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: WerkGesellschaft;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.Bestellung_WerkGesellschaft:
LOAD Distinct
	*
FROM
	[$(vStdTaskQvdExtract)\Einkaufstracker.WerkGesellschaft.qvd]
	(qvd)
;


Left Join
(Einkaufstracker.Bestellung_tmp)
LOAD Distinct
	WerkNr,
	Gesellschaft,
	WerkStandort,
	WerkName
Resident
	Einkaufstracker.Bestellung_WerkGesellschaft
;



Map.Einkaufstracker.Bestellung_Lager:
Mapping
LOAD Distinct
	LagerNr				as IN,
	LagerName			as OUT
Resident
	Einkaufstracker.Bestellung_WerkGesellschaft
;


// Clear
CALL subCommonDropTable('Einkaufstracker.Bestellung_WerkGesellschaft');



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: ArtikelNr;
TRACE **************** **************** **************** ****************;

Map.Einkaufstracker.Bestellung_ArtikelNr_Text:
Mapping
LOAD Distinct
	ArtikelNum						as IN,
	ArtikelNr						as OUT
FROM
	[$(vStdTaskQvdExtract)\Kathrein.MaterialStamm.qvd]
	(qvd)
;



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: Final;
TRACE **************** **************** **************** ****************;

//RENAME Field LieferantenNr to tmp_LieferantenNr;


NoConcatenate
Einkaufstracker.Bestellung_pre:
LOAD Distinct
	*,

	ApplyMap('Map.Einkaufstracker.Bestellung_Lager',LagerNr)	as LagerName,

	ApplyMap('Map.Einkaufstracker.Bestellung_ArtikelNr_Text',Num#(tmp_ArtikelNr)	//If(IsNum(tmp_ArtikelNr),Num(tmp_ArtikelNr),Trim(tmp_ArtikelNr))
		,Text(tmp_ArtikelNr))									as ArtikelNr,
//	If(Len(Trim(txt_ArtikelNr))>0,txt_ArtikelNr,tmp_ArtikelNr)	as ArtikelNr,
	Replace(tmp_Waehrung,'RMB','CNY')							as Waehrung,
	If(Len(Trim(tmp_UserID_Besteller))>0,tmp_UserID_Besteller
		,Text(StrategischerEinkaeufer))							as UserID_Besteller,

	// tmp
	If(Len(Trim(tmp_Waehrung))>0,1,0)							as Flag_Waehrung,
	If(IsNum(Bestelldatum),1,0)									as Flag_Datum,
	WerkNr &'#'& BestellNr &'#'& BestellPosition				as key_Werk_Bestellung_Nr_Pos,
	If(SubField(FILE_NAME,'-',2)='E_Tracker'
		,Num(SubField(FILE_NAME,'-',1)))						as date_FILE_NAME

Resident
	Einkaufstracker.Bestellung_tmp
Where
		Len(Trim(Löschkennzeichen))=0	//not Match(Upper(Löschkennzeichen),'L','S')
;


Left Join
(Einkaufstracker.Bestellung_pre)
LOAD Distinct
	key_Werk_Bestellung_Nr_Pos,
	Num(Max(date_FILE_NAME))									as date_FILE_NAME,
	1															as date_Flag
Resident
	Einkaufstracker.Bestellung_pre
Group By
	key_Werk_Bestellung_Nr_Pos
;


NoConcatenate
Einkaufstracker.Bestellung:
LOAD Distinct
	*
Resident
	Einkaufstracker.Bestellung_pre
Where
		(WerkNr<>'SMU' or date_Flag=1)
;


// Clear
CALL subCommonDropTable('Einkaufstracker.Bestellung_tmp');
CALL subCommonDropTable('Einkaufstracker.Bestellung_pre');



TRACE **************** **************** **************** ****************;
TRACE *** Bestellung: BANF;
TRACE **************** **************** **************** ****************;

Left Join
(Einkaufstracker.Bestellung)
LOAD Distinct
	WerkNr &'#'& BestellNr &'#'& BestellPosition				as key_Werk_Bestellung_Nr_Pos,
	Banf_Nr,
	Banf_Position
FROM
	[$(vStdTaskQvdExtract)\Kathrein.BANF.qvd]
	(qvd)
;

