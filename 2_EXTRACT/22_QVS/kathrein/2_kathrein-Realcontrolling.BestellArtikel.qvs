// ******** EXTRACT - Realcontrolling.BestellArtikel ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-04-25	1.00		created
// 2018-05-15	1.01		add Fields: Kommentar, LaufzeitFixiert
// 2018-05-17	1.02		mod: Kathrein Einkauf (Preiseinheit)
// 2018-05-18	1.03		add: Kathrein Einkauf Menge
// 2018-06-23	1.04		mod: Kathrein
// 2018-06-27	1.05		mod Field: Abschlussbericht
// 2018-06-27	1.06		add Field: LieferantenID
// 2018-06-28	1.07		add Field: Quelle
// 2018-06-30	1.08		modified
// 2018-07-03	1.10		modified
// 2018-07-03	1.11		mod: LieferantMatching
// 2018-07-10	1.20		modified
// 2018-07-12	1.21		modified
// 2018-07-16	1.22		modified
//
// ******** EXTRACT - Realcontrolling.BestellArtikel ********



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Prepare;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.BestellArtikel_tmp:
LOAD * Inline [
	FILE_NAME, Laufzeit, Mengeneinheit, Kommentar
];


Map.Realcontrolling.BestellArtikel_ArtikelNr_Text:
Mapping
LOAD Distinct
	ArtikelNum						as IN,
	ArtikelNr						as OUT
FROM
	[$(vStdTaskQvdExtract)\Kathrein.MaterialStamm.qvd]
	(qvd)
;



// **************** ****************
// *** LieferantMatching
// **************** ****************

Map.Realcontrolling.BestellArtikel_LieferantMatching:
Mapping
LOAD Distinct
	WerkNr &'#'& LieferantenID		as IN,
	LieferantenNr					as OUT
FROM
	[$(vStdTaskQvdExtract)\Kathrein.LieferantMatching.qvd]
	(qvd)
Where
		Len(Trim(WerkNr))>0
;



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel;
TRACE **************** **************** **************** ****************;

Concatenate
(Realcontrolling.BestellArtikel_tmp)
LOAD
	FileName()																					as FILE_NAME,

	'direktes Material'																			as Abschlussbericht,
	If(Len(Trim(V))>0,Num(V)
		,If(Year(X)>=2002,Null(),12))															as Laufzeit,
	Date(O)																						as DatumStartArtikel,
	If(Year(X)>=2002,Date(X))																	as DatumEndeArtikel,

	If(Match(Lower(Trim(E)),'kathrein'),'Kathrein')												as file_Gesellschaft,
	If(not Match(Lower(Trim(E)),'kathrein'),E)													as WerkNr,
	Text(U)																						as Warengruppe_Code,
	Text(C)																						as ArtikelNr,
	D																							as ArtikelName,
	If(Lower(Trim(T))<>'jeder'
		,ApplyMap('Map.Realcontrolling.BestellArtikel_LieferantMatching',Upper(E) &'#KWE#'& Num(T)
		,Text(PurgeChar(T,'#'))))																as LieferantenNr,
	If(Left(T,1)<>'#','KWE#'& Num(T))															as LieferantenID,

	If(Len(Trim(S))>0,'Menge','Volumen')														as ArtEinsparung,
	If(Lower(Trim(T))='jeder','Y','N')															as SecondSource,
	If(Year(X)>=2002,'Y','N')																	as LaufzeitFixiert,
	Num(Q)																						as EinsparungProJahr,
	F																							as Mengeneinheit,
	Num(G)																						as MengeProJahr,
	Num(H)																						as Preiseinheit,
	I																							as Waehrung,
	If(Len(Trim(S))=0,Num(K))																	as Preis_Alt,
	If(Len(Trim(S))=0 and N<>0,Num(N))															as Preis_Neu,
	If(Len(Trim(S))>0,Num(S))																	as Pauschale,
	W																							as Kommentar,
	A																							as Quelle

FROM
	[$(vStdTaskDiv)\Abschlussbericht\??????-Abschlussreport_Direkt.xlsx]
	(ooxml, no labels, table is [Auswertung - Artikel&Positionen])
Where
		Len(Trim(C))>0 and not Match(C,'Artikelnr.')
;



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Werk/Gesellschaft;
TRACE **************** **************** **************** ****************;

Left Join
(Realcontrolling.BestellArtikel_tmp)
LOAD Distinct
	WerkNr,
	Gesellschaft																				as werk_Gesellschaft,
	WerkStandort,
	WerkName
FROM
	[$(vStdTaskQvdExtract)\Einkaufstracker.WerkGesellschaft.qvd]
	(qvd)
;



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Final;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.BestellArtikel:
LOAD
	*,

	If(Len(Trim(werk_Gesellschaft))=0 and Len(Trim(file_Gesellschaft))>0,file_Gesellschaft
		,werk_Gesellschaft)																		as Gesellschaft,
	If(Len(Trim(WerkNr))=3,1,0)																	as Flag_WerkNr

Resident
	Realcontrolling.BestellArtikel_tmp;
;


// Clear
CALL subCommonDropTable('Realcontrolling.BestellArtikel_tmp');
