// ******** EXTRACT - Realcontrolling.Manuell ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-05-15	1.00		created
// 2018-06-23	1.01		mod: Kathrein
// 2018-06-27	1.02		mod: Kathrein
// 2018-06-28	1.03		add Field: Quelle
// 2018-07-10	1.10		modified
// 2018-07-12	1.11		modified
// 2018-07-16	1.12		modified
// 2018-07-18	1.13		rem: Kathrein
//
// ******** EXTRACT - Realcontrolling.Manuell ********



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Prepare;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.Manuell_tmp:
LOAD * Inline [
	FILE_NAME
];



// **************** ****************
// *** LieferantMatching
// **************** ****************

Map.Realcontrolling.Manuell_LieferantMatching:
Mapping
LOAD Distinct
	WerkNr &'#'& LieferantenID		as IN,
	LieferantenNr					as OUT
FROM
	[$(vStdTaskQvdExtract)\Kathrein.LieferantMatching.qvd]
	(qvd)
Where
		Len(Trim(WerkNr))>0
;



TRACE **************** **************** **************** ****************;
TRACE *** Manuell;
TRACE **************** **************** **************** ****************;

Concatenate
(Realcontrolling.Manuell_tmp)
LOAD

	FileName()																		as FILE_NAME,
	If(Len(Trim([Artikelnr.]))>0,'direktes Material','indirektes Material')			as Abschlussbericht,
	If(Lower(Trim(Werk))='kathrein se','KWE',Text(Trim(Werk)))						as WerkNr,

	Text([Artikelnr.])																as ArtikelNr,
	Bezeichnung																		as ArtikelName,

//	ApplyMap('Map.Realcontrolling.Manuell_LieferantMatching',Upper(E) &'#KWE#'& Num([RC-Daten])
//		,Text([RC-Daten]))															as LieferantenNr,
	Text([RC-Daten])																as LieferantenNr,
	'KWE#'& Num([RC-Daten])															as LieferantenID,
	[Bisherige Konditionen]															as LieferantenName,

	If(IsNum(F14),Date(F14),Date(F22))												as DatumStart,
	If(IsNum(F23),Date(F23),If(IsNum(F14),Date(F14),Date(F22)))						as DatumEnde,
	If(IsNum(F24),Date(F24)
		,If(Len(Trim(F26))=0 and Len(Trim(F27))=0,Date(F23)))						as DatumRealisiert,

	Kommentar,
	Zuständigkeit																	as Quelle,
	[Mengen-Einheit]																as Mengeneinheit,
	Menge																			as MengePlan,
	Null()																			as MengeRealisiert,

	Währung																			as Waehrung,
	[Preis-
einheit]																			as Preiseinheit,
	Num(F10)																		as Preis_Alt,
	Num(F13)																		as Preis_Neu,
	Num(Einsparung)																	as EinsparungPlan,
	If(IsNum(F24) and Len(Trim(F25))>0,Num(F25)
		,If(Len(Trim(F26))=0 and Len(Trim(F27))=0,Num(Einsparung)))					as EinsparungRealisiert,

	If(IsNum(F26) and Len(Trim(F27))>0,Num(F26))									as Pauschale,
	If(IsNum(F26) and Len(Trim(F27))>0,F27)											as Frequenz_Intervall,
	If(IsNum(F26) and Len(Trim(F27))>0,'E')											as Frequenz_AnfangEnde

FROM
	[$(vStdTaskDiv)\Abschlussbericht\??????-Abschlussreport_Indirekt*.xlsx]
	(ooxml, embedded labels, table is [Auswertung - Indirekt])
Where
		Lower(Left(Werk,4))<>'werk'
	and
		not Match(Lower(Trim([Preis-
einheit])),'summe','[t.b.d.]')
;



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Werk/Gesellschaft;
TRACE **************** **************** **************** ****************;

Left Join
(Realcontrolling.Manuell_tmp)
LOAD Distinct
	WerkNr,
	Gesellschaft																	as werk_Gesellschaft,
	WerkStandort,
	WerkName
FROM
	[$(vStdTaskQvdExtract)\Einkaufstracker.WerkGesellschaft.qvd]
	(qvd)
;



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Final;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.Manuell_tmp:
LOAD
	*,

	If(Len(Trim(werk_Gesellschaft))>0,werk_Gesellschaft,'Kathrein')				as Gesellschaft

Resident
	Realcontrolling.Manuell_tmp
;


// Clear
CALL subCommonDropTable('Realcontrolling.Manuell_tmp');
