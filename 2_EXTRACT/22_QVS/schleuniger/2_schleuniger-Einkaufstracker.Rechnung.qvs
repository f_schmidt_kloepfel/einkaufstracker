// ******** EXTRACT - Einkaufstracker.Rechnung ********
// 
// author:		Gregor Passon, Felix Schmidt
// 
//
// LOG:
// modified		version		description
// 2018-01-29	1.00		created
// 2018-02-01	1.10		modified
// 2018-02-12	2.00		modified
//
// ******** EXTRACT - Einkaufstracker.Rechnung ********



TRACE **************** **************** **************** ****************;
TRACE *** Rechnung;
TRACE **************** **************** **************** ****************;

//NoConcatenate
Einkaufstracker.Rechnung:
Add
LOAD
	FileName()							as FILE_NAME,

	floor(num(left(WerkNr,4)))			as WerkNr,
	floor(num(left(WerkNr,4)))			as WerkName,

	Text(Null())						as ArtikelNr,
	Null()								as Warengruppe_Code,
	Text(LieferantenNr)					as LieferantenNr,

	Text(Interne_RechnungsNr)			as Interne_RechnungsNr,
	Text(Externe_RechnungsNr)			as Externe_RechnungsNr,
	Null()								as RechnungsPosition,
	Null()								as RechnungsMenge,
	Null()								as Mengeneinheit,
	Null()								as RechnungsPreis,
	Null()								as RechnungsRabatt,
	Null()								as RechnungsPreis_Abzueglich_Rabatt,
	Null()								as Preiseinheit,
	Null()								as Beschaffungsnebenkosten,
	Num(RechnungsWert)*(-1)				as RechnungsWert,
	W�hrung								as Waehrung,
	Null()								as Zahlungskondition,
	Eingangsdatum,
	Zahldatum,
	Null()								as BestellNr,
	Null()								as BestellPosition,
	Null()								as SachkontoNr,
	Null()								as Sachkontobezeichnung,
	Null()								as KostenstellenNr,
	Null()								as KostenstellenBezeichnung
FROM
[$(vStdTaskDiv)\Kreditor\Kreditorenums�tze_Skonto_*.xlsx]
(ooxml, embedded labels, filters(
	Remove(Row, Pos(Top, 4)),
	Remove(Row, Pos(Top, 3)),
	Remove(Row, Pos(Top, 2))
))
Where	Year(Zahldatum)<=Year(Today())+2
;


Left Join
(Einkaufstracker.Rechnung)
LOAD
	WerkNr,
	Bereich,			//Sonderlocke
	Gesellschaft,
	WerkStandort,
	WerkAnschrift		//Sonderlocke
FROM
[$(vStdTaskQvdExtract)\Einkaufstracker.WerkGesellschaft.qvd]
(qvd)
;



TRACE **************** **************** **************** ****************;
TRACE *** Standort China;
TRACE **************** **************** **************** ****************;

concatenate
(Einkaufstracker.Rechnung)
LOAD
	FileName()												as FILE_NAME,

	WerkName												as Gesellschaft,
	'Tianjin'												as WerkStandort,
	WerkNr,
	WerkNr													as WerkName,

	Text(Replace(ArtikelNr,'.000000',''))					as ArtikelNr,
	Null()													as Warengruppe_Code,
	Text(Replace(LieferantenNr,'.000000',''))				as LieferantenNr,

	Text(Replace(Interne_RechnungsNr,'.000000',''))			as Interne_RechnungsNr,
	Text(Replace(Externe_RechnungsNr,'.000000',''))			as Externe_RechnungsNr,
	RechnungsPosition, 
	num(Replace(Mengeneinheit,'.',','))						as RechnungsMenge,
	RechnungsPreis,
	RechnungsRabatt,
	num(Replace(RechnungsPreis_Abz�glich_Rabatt,'.',','))	as RechnungsPreis_Abzueglich_Rabatt, 
	1														as Preiseinheit,
	Preiseinheit   											as Mengeneinheit,
	Beschaffungsnebenkosten, 
	RechnungsWert,
	W�hrung													as Waehrung,
	Zahlungskondition,
	date(Replace(Eingangsdatum,'.000000',''))				as Eingangsdatum,
	date(Replace(Zahldatum,'.000000',''))					as Zahldatum,
	Null()													as BestellNr,
	Null()													as BestellPosition,
	SachkontoNr,
	Sachkontobezeichnung,
	KostenstellenNr,
	KostenstellenBezeichnung
FROM
[$(vStdTaskDiv)\Kompletter Datensatz-STJ.xlsx]
(ooxml, embedded labels, header is 3 lines, table is [Rechnungsdaten | Invoice Data], filters(
	Remove(Row, Pos(Top, 3)),
	Remove(Row, Pos(Top, 2))
))
Where	Year(Date(Replace(Zahldatum,'.000000','')))<=Year(Today())+2
;

