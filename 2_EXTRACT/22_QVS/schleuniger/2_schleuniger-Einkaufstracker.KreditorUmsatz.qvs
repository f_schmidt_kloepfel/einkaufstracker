// ******** EXTRACT - Einkaufstracker.KreditorUmsatz ********
// 
// author:		Felix Schmidt
// 
//
// LOG:
// modified		version		description
// 2018-02-12	2.00		modified
//
// ******** EXTRACT - Einkaufstracker.KreditorUmsatz ********



TRACE **************** **************** **************** ****************;
TRACE *** KreditorUmsatz: STJ;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.KreditorUmsatz:
LOAD
	FileName()								as FILE_NAME,

	Gesellschaft,
	'Tianjin'								as WerkStandort,
	WerkNr,
	WerkName,

	Null()									as Warengruppe_Code,
	Text(KreditorenNr)						as KreditorenNr,
	W�hrung									as Waehrung,
	[2014(/2015) netto]						as netto_2014,
	[2014(/2015) skonto]					as skonto_2014,
	[2015(/2016) netto]						as netto_2015,
	[2015(/2016) skonto]					as skonto_2015,
	[2016(/2017) netto]						as netto_2016,
	[2016(/2017) skonto]					as skonto_2016,
	[2017(/2018) netto]						as netto_2017,
	[2017(/2018) skonto]					as skonto_2017

FROM
[$(vStdTaskDiv)\Kompletter Datensatz-STJ.xlsx]
(ooxml, embedded labels, header is 3 lines, table is [Kreditorendaten | Creditor Data], filters(
Remove(Row, Pos(Top, 3)),
Remove(Row, Pos(Top, 2))
));



TRACE **************** **************** **************** ****************;
TRACE *** KreditorUmsatz: Saldenliste;
TRACE **************** **************** **************** ****************;

FOR vKreditorUmsatzJahr=2014 to 2017

	Concatenate
	(Einkaufstracker.KreditorUmsatz)
	LOAD
		FileName()							as FILE_NAME,
		Text(Konto)							as KreditorenNr,
//		[Bezeichnung Konto],
		Lieferant							as KreditorenName,
//		[Letzte Bewegung],
		WKz									as Waehrung,
//		[EB Saldo],
//		[Periode Soll],
//		[Periode Haben],
//		[Kumuliert Soll],
//		[Kumuliert Haben],
		Num([Kumuliert Saldo])				as [netto_$(vKreditorUmsatzJahr)]
//		[(%) vom Umsatz],
//		[FW WKz],
//		[FW EB Saldo],
//		[FW Periode Soll],
//		[FW Periode Haben],
//		[FW Kumuliert Soll],
//		[FW Kumuliert Haben],
//		[FW Kumuliert Saldo]
	FROM
	[$(vStdTaskDiv)\Umsatz\Saldenliste Lieferanten $(vKreditorUmsatzJahr).xls]
	(biff, embedded labels, table is rptFibuSaldenliste$)
	;

NEXT vKreditorUmsatzJahr


// Clear
LET vKreditorUmsatzJahr		= Null();



TRACE **************** **************** **************** ****************;
TRACE *** KreditorUmsatz: Kreditorenumsatz;
TRACE **************** **************** **************** ****************;

FOR vKreditorUmsatzJahr=2014 to 2017

	Concatenate
	(Einkaufstracker.KreditorUmsatz)
	LOAD
		FileName()							as FILE_NAME,
		'Schleuniger GmbH'					as Gesellschaft,
		If(BuKr=4110,'Thun'
			,If(BuKr=4600,'Radevormwald'))	as WerkStandort,
		Text(BuKr)							as WerkNr,
		Text(BuKr)							as WerkName,
//		Abstimmkto,
		Text(Kreditor)						as KreditorenNr,
		[Name 1]							as KreditorenName,
		Lnd,
		Postleitz.,
		Ort,
		Stra�e,
//		Rg,
		W�hrg								as Waehrung,
		Num(Einkauf)						as [netto_$(vKreditorUmsatzJahr)]
	FROM
	[$(vStdTaskDiv)\Umsatz\Kreditorenumsatz Gesamt.xlsx]
	(ooxml, embedded labels, header is 3 lines, table is [4110_2014])
	Where	Match(BuKr,4110,4600)
	;

NEXT vKreditorUmsatzJahr


// Clear
LET vKreditorUmsatzJahr		= Null();



TRACE **************** **************** **************** ****************;
TRACE *** KreditorUmsatz: Kreditoren;
TRACE **************** **************** **************** ****************;

Concatenate
(Einkaufstracker.KreditorUmsatz)
LOAD
	FileName()							as FILE_NAME,
	Text(Kreditor)						as KreditorenNr,
	Name								as KreditorenName,
	'EUR'								as Waehrung,
	[2014]								as netto_2014,
//	[2014(/2015) skonto]				as skonto_2014,
	[2015]								as netto_2015,
//	[2015(/2016) skonto]				as skonto_2015,
	[2016]								as netto_2016,
//	[2016(/2017) skonto]				as skonto_2016,
	[2017]								as netto_2017,
//	[2017(/2018) skonto]				as skonto_2017,
//	[2013],
//	[2012],
	[D-A-CH],
	CST
FROM
[$(vStdTaskDiv)\Umsatz\Kreditoren 2012_2017.xlsx]
(ooxml, embedded labels, header is 1 lines, table is [Kreditoren 2012-2017])
Where	Kreditor<>'Gesamtergebnis'
;


Concatenate
(Einkaufstracker.KreditorUmsatz)
LOAD
	FileName()							as FILE_NAME,
	'Schleuniger GmbH'					as Gesellschaft,
	If(BuKr=4110,'Thun'
		,If(BuKr=4600,'Radevormwald'))	as WerkStandort,
	Text(BuKr)							as WerkNr,
	Text(BuKr)							as WerkName,
//	Abstimmkto, 
	Text(Kreditor)						as KreditorenNr,
	[Name 1]							as KreditorenName,
	Lnd,
	Postleitz.,
	Ort,
	Stra�e,
//	Rg,
	W�hrg								as Waehrung,
	Num(Einkauf)						as [netto_2014]
FROM
[$(vStdTaskDiv)\Umsatz\Schleuniger 4110 und 4600_ 2014_Kreditoren Umsatz.xlsx]
(ooxml, embedded labels, header is 3 lines, table is [2014_Kreditoren Umsatz])
;


