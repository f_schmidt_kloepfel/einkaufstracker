// ******** TRANSACT - Einkaufstracker.DebitorUmsatz ********
// 
// author:		Felix Schmidt
// 
//
// LOG:
// modified		version		description
// 2017-06-20	1.00		created
// 2017-09-20	2.00		mod: Template.xlsx
// 2017-09-20	2.10		mod: Werk/Gesellschaft; add: FileSize
// 2017-10-25	3.00		modified
// 2017-11-20	4.00		modified
// 2017-11-20	4.01		add Field: Key_Umsatz; mod Field: Debitor_UmsatzJahr
// 2017-11-24	4.10		mod: Kalender
// 2017-11-26	4.11		mod: Kalender
// 2018-01-20	4.20		add Field: Key_WerkGesellschaft_Lager
// 2018-02-01	4.22		mod: Waehrung/Wechselkurs
// 2018-02-12	4.30		add: Wechselkurs
//
// ******** TRANSACT - Einkaufstracker.DebitorUmsatz ********



TRACE **************** **************** **************** ****************;
TRACE *** Mapping;
TRACE **************** **************** **************** ****************;


// **************** ****************
// *** Waehrung
// **************** ****************

NoConcatenate
Einkaufstracker.DebitorUmsatz_Wechselkurs:
LOAD Distinct
	*
FROM
[$(vStdTaskQvdTransact)\Einkaufstracker.Wechselkurs.qvd]
(qvd)
Where	Flag_Gueltig=1
and		Flag_GueltigGeschaeftsjahr=1
;

Map.Einkaufstracker.DebitorUmsatz_Wechselkurs_Key:
Mapping
LOAD Distinct
	Key_Iso4217_Alpha_Geschaeftsjahr		as IN,
	Key_Wechselkurs							as OUT
Resident
Einkaufstracker.DebitorUmsatz_Wechselkurs
;

Map.Einkaufstracker.DebitorUmsatz_Wechselkurs_ReRate:
Mapping
LOAD Distinct
	Key_Iso4217_Alpha_Geschaeftsjahr		as IN,
	Wechselkurs_ReRate						as OUT
Resident
Einkaufstracker.DebitorUmsatz_Wechselkurs
;



TRACE **************** **************** **************** ****************;
TRACE *** Umsatz;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.DebitorUmsatz_load:
LOAD Distinct
	*,
	If(Len(Trim(Waehrung))=3,If(Upper(Trim(Waehrung))='RMB','CNY',Upper(Trim(Waehrung))),'EUR')		as Key_Iso4217_Alpha,
	RowNo()																							as DebitorUmsatz_UmsatzId
FROM
[$(vStdTaskQvdExtract)\Einkaufstracker.DebitorUmsatz.qvd]
(qvd)
;


NoConcatenate
Einkaufstracker.DebitorUmsatz_tmp:
LOAD Distinct

	Key_WerkGesellschaft &'##'								as Key_WerkGesellschaft_Lager,
	If(Len(Trim(Key_WerkGesellschaft))>3,1,0)				as Flag_WerkGesellschaft,
	*

;
LOAD

	// Keys
	WerkGesellschaft_Gesellschaft
		&'#'& WerkGesellschaft_WerkStandort
		&'#'& WerkGesellschaft_WerkNr
		&'#'& WerkGesellschaft_WerkName						as Key_WerkGesellschaft,

	// Fields
	*
;
LOAD

	// Keys
	Key_Iso4217_Alpha,

	// Flag

	// WerkGesellschaft
	If(Len(Trim(Gesellschaft))>0,Gesellschaft,Null())		as WerkGesellschaft_Gesellschaft,
	If(Len(Trim(WerkStandort))>0,WerkStandort,Null())		as WerkGesellschaft_WerkStandort,
	If(Len(Trim(WerkNr))>0,WerkNr,Null())					as WerkGesellschaft_WerkNr,
	If(Len(Trim(WerkName))>0,WerkName,Null())				as WerkGesellschaft_WerkName,
	Null()													as WerkGesellschaft_LagerNr,
	Null()													as WerkGesellschaft_LagerName,

	// DebitorUmsatz
	DebitorUmsatz_UmsatzId,
	Waehrung												as DebitorUmsatz_Waehrung,
	[2014],
	[2015],
	[2016],
	[2017]

Resident
Einkaufstracker.DebitorUmsatz_load
;


// Clear
CALL subCommonDropTable('Einkaufstracker.DebitorUmsatz_load');



TRACE **************** **************** **************** ****************;
TRACE *** CrossTable;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.DebitorUmsatz_pre:
LOAD Distinct

	// Keys
	Key_WerkGesellschaft,
	Key_WerkGesellschaft_Lager,
	Key_Iso4217_Alpha,

	// Flag
	Flag_WerkGesellschaft,

	// WerkGesellschaft
	WerkGesellschaft_Gesellschaft,
	WerkGesellschaft_WerkStandort,
	WerkGesellschaft_WerkNr,
	WerkGesellschaft_WerkName,
	WerkGesellschaft_LagerNr,
	WerkGesellschaft_LagerName,

	// DebitorUmsatz
	DebitorUmsatz_UmsatzId,
	DebitorUmsatz_Waehrung

Resident
Einkaufstracker.DebitorUmsatz_tmp
;


Einkaufstracker.DebitorUmsatz_cross:
CrossTable(DebitorUmsatz_Jahr,DebitorUmsatz_WertOriginal,1)
LOAD
	// Keys
	DebitorUmsatz_UmsatzId,
	// Fields
	[2014],
	[2015],
	[2016],
	[2017]
Resident
Einkaufstracker.DebitorUmsatz_tmp
;


// Final
Left Join
(Einkaufstracker.DebitorUmsatz_pre)
LOAD
	DebitorUmsatz_UmsatzId,
	Num#(DebitorUmsatz_Jahr)															as DebitorUmsatz_Jahr,
	Num(DebitorUmsatz_WertOriginal)														as DebitorUmsatz_WertOriginal
Resident
Einkaufstracker.DebitorUmsatz_cross
Where	(IsNum(DebitorUmsatz_WertOriginal) and DebitorUmsatz_WertOriginal<>0)
;


// Clear
CALL subCommonDropTable('Einkaufstracker.DebitorUmsatz_cross');
CALL subCommonDropTable('Einkaufstracker.DebitorUmsatz_tmp');



TRACE **************** **************** **************** ****************;
TRACE *** Kalender;
TRACE **************** **************** **************** ****************;

Left Join
(Einkaufstracker.DebitorUmsatz_pre)
LOAD Distinct
	Left(Kalender_GJ_JahrNum,4)			as DebitorUmsatz_Jahr,
	Key_Kalender_Geschaeftsjahr,
	Kalender_GJ_JahrNum,
	Kalender_GJ_Von,
	Kalender_GJ_Bis
FROM
[$(vStdTaskQvdTransact)\KC.Kalender.qvd]
(qvd)
;



TRACE **************** **************** **************** ****************;
TRACE *** DebitorUmsatz;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.DebitorUmsatz:
LOAD Distinct
	RowNo()																									as Key_DebitorUmsatz,
	*,
	Num(Debitor_UmsatzWaehrungReRate*DebitorUmsatz_WertOriginal)											as DebitorUmsatz_Wert
;
LOAD
	*,
	ApplyMap('Map.Einkaufstracker.DebitorUmsatz_Wechselkurs_Key',Key_Iso4217_Alpha_Geschaeftsjahr,1)		as Key_Wechselkurs,
	ApplyMap('Map.Einkaufstracker.DebitorUmsatz_Wechselkurs_ReRate',Key_Iso4217_Alpha_Geschaeftsjahr,1)		as Debitor_UmsatzWaehrungReRate
;
LOAD

	// Keys
	Key_Kalender_Geschaeftsjahr &'#'& Key_Iso4217_Alpha &'#'& Key_WerkGesellschaft							as Key_Umsatz,
	Key_WerkGesellschaft,
//	Key_WerkGesellschaft_Lager,
	Key_WerkGesellschaft &'##'																				as Key_WerkGesellschaft_Lager,
	Key_Iso4217_Alpha &'#'& Key_Kalender_Geschaeftsjahr														as Key_Iso4217_Alpha_Geschaeftsjahr,
	Key_Iso4217_Alpha,
	Key_Kalender_Geschaeftsjahr,

	// Flag
	Flag_WerkGesellschaft,
	Flag_Gesellschaft,
	Flag_Standort,
	Flag_WerkNr,
	Flag_WerkName,

	// WerkGesellschaft
	WerkGesellschaft_Gesellschaft,
	WerkGesellschaft_WerkStandort,
	WerkGesellschaft_WerkNr,
	WerkGesellschaft_WerkName,
	WerkGesellschaft_LagerNr,
	WerkGesellschaft_LagerName,

	// Kalender
	Kalender_GJ_JahrNum,
	Kalender_GJ_Von,
	Kalender_GJ_Bis,

	// Umsatz
	DebitorUmsatz_UmsatzId,
	DebitorUmsatz_Waehrung,
	DebitorUmsatz_WertOriginal

Resident
Einkaufstracker.DebitorUmsatz_pre
;


// Clear
CALL subCommonDropTable('Einkaufstracker.DebitorUmsatz_pre');



TRACE **************** **************** **************** ****************;
TRACE *** DebitorUmsatz: Wechselkurs;
TRACE **************** **************** **************** ****************;

FOR Each vDebitorUmsatzWaehrung in $(vCalcWaehrungRate)

	NoConcatenate
	[Einkaufstracker.DebitorUmsatz_Wechselkurs_$(vDebitorUmsatzWaehrung)]:
	LOAD Distinct
		Key_Wechselkurs,
		Key_Iso4217_Alpha,
		Key_Kalender_Geschaeftsjahr,
		Key_Iso4217_Alpha_Geschaeftsjahr,
		Wechselkurs_Rate,
		Wechselkurs_ReRate
	Resident
	Einkaufstracker.DebitorUmsatz_Wechselkurs
	Where	Key_Iso4217_Alpha='$(vDebitorUmsatzWaehrung)'
	;


	Left Join
	([Einkaufstracker.DebitorUmsatz_Wechselkurs_$(vDebitorUmsatzWaehrung)])
	LOAD Distinct
		Key_Kalender_Geschaeftsjahr,
		Key_DebitorUmsatz,
		Key_Iso4217_Alpha															as tmp_Key_Iso4217_Alpha,
		DebitorUmsatz_Wert,
		DebitorUmsatz_WertOriginal
	Resident
	Einkaufstracker.DebitorUmsatz
	;


	Left Join
	(Einkaufstracker.DebitorUmsatz)
	LOAD Distinct
		Key_DebitorUmsatz,
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,DebitorUmsatz_WertOriginal
			,Wechselkurs_Rate*DebitorUmsatz_Wert),$(vStdFormatNumberDec))			as [DebitorUmsatz_Wert_$(vDebitorUmsatzWaehrung)]
	Resident
	[Einkaufstracker.DebitorUmsatz_Wechselkurs_$(vDebitorUmsatzWaehrung)]
	;


	// Clear
	CALL subCommonDropTable('Einkaufstracker.DebitorUmsatz_Wechselkurs_$(vDebitorUmsatzWaehrung)');

NEXT vDebitorUmsatzWaehrung


// Clear
CALL subCommonDropTable('Einkaufstracker.DebitorUmsatz_Wechselkurs');
LET vDebitorUmsatzWaehrung		= Null();
