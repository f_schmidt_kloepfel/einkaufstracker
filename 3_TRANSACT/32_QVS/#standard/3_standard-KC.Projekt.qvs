// ******** TRANSACT - KC.Projekt ********
// 
// author:		Felix Schmidt
// 
//
// LOG:
// modified		version		description
// 2017-09-20	1.00		created
// 2017-09-23	1.10		mod: GeschäftsJahr
// 2017-09-28	1.20		add: FileName
// 2017-10-12	2.00		modified
// 2017-11-08	2.10		mod Field: Projekt_GueltigBis
// 2017-11-22	2.20		add Fields: Projekt_DatumEnde, Projekt_DauerInWochenInput
// 2018-01-15	2.30		mod Field: Projekt_Name
// 
// ******** TRANSACT - KC.Projekt ********



// **************** **************** **************** ****************
// *** Extract
// **************** **************** **************** ****************

NoConcatenate
KC.Projekt_load:
LOAD Distinct
	Projekt_Variable,
	Projekt_Data
;
LOAD
	Projekt_Variable,
	Projekt_Data
FROM
[$(vStdTaskQvdExtract)\KC.Projekt.qvd]
(qvd)
Where	Len(Trim(Projekt_Variable))>0
and		Len(Trim(Projekt_Data))>0
;



// **************** **************** **************** ****************
// *** CrossTable
// **************** **************** **************** ****************


// **************** ****************
// *** Name
// **************** ****************

NoConcatenate
KC.Projekt_tmp:
LOAD Distinct
	Projekt_Data				as Projekt_Name
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Name')
;


IF not NoOfRows('KC.Projekt_tmp')>0 then

	Concatenate
	(KC.Projekt_tmp)
	LOAD * Inline [
		Projekt_Name
		'$(vStdTaskScope)'
	];

ENDIF



// **************** ****************
// *** Projekt
// **************** ****************

Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Date(Floor(Projekt_Data))	as Projekt_DatumStart
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Start')
;

Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Date(Floor(Projekt_Data))	as Projekt_DatumEnde
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Ende')
;

Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Projekt_Data				as Projekt_DauerInWochenInput
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Dauer')
;

Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Projekt_Data				as Projekt_GeoAdresse
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Adresse')
;

Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Projekt_Data				as Projekt_GeoPlz
Resident
KC.Projekt_load
Where	Index(Upper(Projekt_Variable),'PLZ')
;

Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Projekt_Data				as Projekt_GeoOrt
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Ort')
;


// GeschäftsJahr
Left Join
(KC.Projekt_tmp)
LOAD Distinct
	Projekt_GeschaeftsJahrInput,
	Num(Projekt_GeschaeftsJahrMonat,'00')	as Projekt_GeschaeftsJahrMonat,
	Num(Projekt_GeschaeftsJahrTag,'00')		as Projekt_GeschaeftsJahrTag,
	If(Projekt_GeschaeftsJahrMonat=1 and Projekt_GeschaeftsJahrTag=1,0
		,1)									as Flag_GeschaeftsJahr
;
LOAD
	Projekt_GeschaeftsJahrInput,
	If(IsNum(tmp_Monat),Num(tmp_Monat),1)	as Projekt_GeschaeftsJahrMonat,
	If(IsNum(tmp_Tag),Num(tmp_Tag),1)		as Projekt_GeschaeftsJahrTag
;
LOAD
	Projekt_GeschaeftsJahrInput,
	If(not IsNull(tmp_Delimiter),SubField(Projekt_GeschaeftsJahrInput,tmp_Delimiter,2)
		,Null())							as tmp_Monat,
	If(not IsNull(tmp_Delimiter),SubField(Projekt_GeschaeftsJahrInput,tmp_Delimiter,1)
		,Null())							as tmp_Tag
;
LOAD
	Projekt_Data							as Projekt_GeschaeftsJahrInput,
	If(Index(Projekt_Data,'.'),'.'
		,If(Index(Projekt_Data,'-'),'-'
		,Null()))							as tmp_Delimiter
Resident
KC.Projekt_load
Where	Index(Projekt_Variable,'Geschäftsjahr')
;


// Clear
CALL subCommonDropTable('KC.Projekt_load');



// **************** **************** **************** ****************
// *** Projekt
// **************** **************** **************** ****************

NoConcatenate
KC.Projekt:
LOAD Distinct

	*,
	If(Flag_ProjektWoche=1,If(IsNum(Projekt_DatumEnde),Ceil((Projekt_DatumEnde-Projekt_DatumStart+1)/7)
		,Projekt_DauerInWochenInput))		as Projekt_DauerInWochen

;
LOAD

	// Flag
	Flag_GeschaeftsJahr,
	If(IsNum(Projekt_DatumStart) and (IsNum(Projekt_DauerInWochenInput) or IsNum(Projekt_DatumEnde))
		,1,0)								as Flag_ProjektWoche,

	// Projekt
	Projekt_Name,
	Projekt_DatumStart,
	Projekt_DatumEnde,
	Projekt_DauerInWochenInput,
	Projekt_GeoAdresse,
	Projekt_GeoPlz,
	Projekt_GeoOrt,
	Projekt_GeschaeftsJahrInput,
	Projekt_GeschaeftsJahrMonat,
	Projekt_GeschaeftsJahrTag

Resident
KC.Projekt_tmp
;


// Clear
CALL subCommonDropTable('KC.Projekt_tmp');
