// ******** TRANSACT - Einkaufstracker.GoogleGeocode ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2017-09-04	1.00		created
// 2017-09-21	1.01		modified
// 2017-10-04	2.00		modified
// 2017-10-04	2.01		add: country review
// 2017-11-05	2.02		mod Field: GoogleGeocode_Request
// 2017-11-18	3.00		modified + split: EXTRACT/TRANSACT
// 2018-02-03	3.10		add: Destatis
// 2018-02-20	3.11		mod: Extract
// 2018-05-02	3.20		mod: Destatis
//
// ******** TRANSACT - Einkaufstracker.GoogleGeocode ********



TRACE **************** **************** **************** ****************;
TRACE *** GoogleGeocode: Extract;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.GoogleGeocode_tmp:
LOAD Distinct
	Text(GoogleGeocode_PlaceId)					as Key_GoogleGeocode_Place,
	*
FROM
	[$(vStdTaskQvdExtract)\Einkaufstracker.GoogleGeocode.qvd]
	(qvd)
Where
		Len(GoogleGeocode_PlaceId)>0
	and
		Len(Trim(Key_GoogleGeocode_Request))>0
	and
		Flag_Error=0
;


// ******* ********
// country
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as Key_Iso3166_Alpha2,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_Country
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='country'
;


// ******* ********
// admin1
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_Admin1Short,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_Admin1Long
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='administrative_area_level_1'
;


// ******* ********
// admin2
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_Admin2Short,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_Admin2Long
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='administrative_area_level_2'
;


// ******* ********
// locality
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_LocalityShort,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_LocalityLong
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='locality'
;


// ******* ********
// sublocality
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_LocalitySubShort,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_LocalitySubLong
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='sublocality'
;


// ******* ********
// sublocality level1
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_LocalitySubL1Short,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_LocalitySubL1Long
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='sublocality_level_1'
;


// ******* ********
// postal code
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_PostalCode
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='postal_code'
;


// ******* ********
// route
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_RouteShort,
	GoogleGeocode_AdressComponent_NameLong		as GoogleGeocode_RouteLong
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='route'
;


// ******* ********
// street number
// ******* ********

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct
	Key_GoogleGeocode_Place,
	GoogleGeocode_AdressComponent_NameShort		as GoogleGeocode_StreetNumber
Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		GoogleGeocode_AdressComponent_Type='street_number'
;



TRACE **************** **************** **************** ****************;
TRACE *** GoogleGeocode: Destatis;
TRACE **************** **************** **************** ****************;

Left Join
(Einkaufstracker.GoogleGeocode_tmp)
LOAD Distinct

	// Keys
	Key_Iso3166_Alpha2,
	Key_Iso3166_Alpha3,
	Key_Iso3166_Numeric,
	Key_Continent_Destatis,
	Key_Iso4217_Alpha,
	Key_Iso4217_Numeric,

	// Destatis
	Key_Iso3166_Alpha2						as Destatis_Staat_Iso3166_Alpha2,
	Key_Iso3166_Alpha3						as Destatis_Staat_Iso3166_Alpha3,
	Key_Iso3166_Numeric						as Destatis_Staat_Iso3166_Numeric,
	Key_Iso4217_Alpha						as Destatis_Staat_Iso4217_Alpha,
	Key_Iso4217_Numeric						as Destatis_Staat_Iso4217_Numeric,
	Staat_Name_AmtlicheKurzform				as Destatis_Staat_AmtlicheKurzform,
	Staat_Name_AmtlicheVollform				as Destatis_Staat_AmtlicheVollform,
	Staat_Kontinent							as Destatis_Staat_Kontinent

FROM
	[$(vStdTaskQvdTransactData)\Destatis.Staat.qvd]
	(qvd)
Where
		Len(Trim(Staat_Name_AmtlicheKurzform))>0
;



TRACE **************** **************** **************** ****************;
TRACE *** GoogleGeocode: Final;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.GoogleGeocode:
LOAD Distinct

	// Keys
	Key_Anschrift,
	Key_GoogleGeocode_Place,
	Key_GoogleGeocode_Request,
	Key_Iso3166_Numeric,
	Key_Iso3166_Alpha2,
	Key_Iso3166_Alpha3,
	Key_Continent_Destatis,
	Key_Iso4217_Alpha,
	Key_Iso4217_Numeric,

	// Flag
	If(Peek(Key_Anschrift)<>Key_Anschrift,1,0)	as Flag_PlacePosition,


	// Destatis
	Destatis_Staat_Iso3166_Alpha2,
	Destatis_Staat_Iso3166_Alpha3,
	Destatis_Staat_Iso3166_Numeric,
	Destatis_Staat_Iso4217_Alpha,
	Destatis_Staat_AmtlicheKurzform,
	Destatis_Staat_AmtlicheVollform,
	Destatis_Staat_Kontinent,

	// Google
	GoogleGeocode_PartialMatch,
	GoogleGeocode_PlaceId,
	GoogleGeocode_FormattedAddress,
	GoogleGeocode_LocationLat,
	GoogleGeocode_LocationLng,
	GoogleGeocode_LocationNorthEastLat,
	GoogleGeocode_LocationNorthEastLng,
	GoogleGeocode_LocationSouthWestLat,
	GoogleGeocode_LocationSouthWestLng,
	GoogleGeocode_Country,
	GoogleGeocode_Admin1Short,
	GoogleGeocode_Admin1Long,
	GoogleGeocode_Admin2Short,
	GoogleGeocode_Admin2Long,
	GoogleGeocode_LocalityShort,
	GoogleGeocode_LocalityLong,
	GoogleGeocode_LocalitySubShort,
	GoogleGeocode_LocalitySubLong,
	GoogleGeocode_LocalitySubL1Short,
	GoogleGeocode_LocalitySubL1Long,
	GoogleGeocode_PostalCode,
	GoogleGeocode_RouteShort,
	GoogleGeocode_RouteLong,
	GoogleGeocode_StreetNumber

Resident
	Einkaufstracker.GoogleGeocode_tmp
Where
		Len(Trim(Key_Iso3166_Alpha2))=2
Order By
	Key_Anschrift
	,Key_GoogleGeocode_Place
;


// Clear
CALL subCommonDropTable('Einkaufstracker.GoogleGeocode_tmp');
