// ******** TRANSACT - Realcontrolling.BestellArtikel ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-02-25	1.00		created
// 2018-02-26	1.10		modified
// 2018-02-28	1.20		modified
// 2018-03-11	1.30		modified
// 2018-04-09	1.31		mod Fields: Flag_TrackingArt*
// 2018-04-09	1.32		mod: Error
// 2018-04-16	1.33		mod Field: Flag_TrackingArt
// 2018-04-20	1.40		add: Pauschale
// 2018-05-07	1.50		mod: Preiseinheit
// 2018-05-14	1.51		add Field: Flag_SecondSource
// 2018-05-15	1.52		add Field: BestellArtikel_Kommentar
// 2018-05-15	1.60		mod: Tracking
// 2018-05-17	1.61		mod Fields: BestellArtikel_PreisAlt_Original, BestellArtikel_PreisNeu_Original
// 2018-05-18	1.62		mod: Tracking
// 2018-05-22	1.63		add: subMappingMengeEinheit
// 2018-05-23	1.63		mod: Tracking
// 2018-06-26	1.64		mod Field: BestellArtikel_Pauschale_Original
// 2018-07-03	1.70		mod: Tracking
// 2018-07-04	1.71		mod Fields: BestellArtikel_PreisAlt_Original, BestellArtikel_PreisNeu_Original
// 2018-07-06	1.80		mod Fields: Len(PurgeChar())
// 2018-07-12	1.81		modified
// 2018-07-16	1.82		modified
// 2018-07-17	1.83		mod Fields: Len(PurgeChar()), *Warengruppe*
//
// ******** TRANSACT - Realcontrolling.BestellArtikel ********



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Mapping;
TRACE **************** **************** **************** ****************;

CALL subMappingWaehrungDate;
CALL subMappingArtikelWarengruppe;
CALL subMappingWarengruppeStamm;
CALL subMappingLieferant;
CALL subMappingKreditorLieferantId;
CALL subMappingMengeEinheit;



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Extract;
TRACE **************** **************** **************** ****************;


// **************** ****************
// *** Key
// **************** ****************

NoConcatenate
Realcontrolling.BestellArtikel_tmp:
LOAD
	*,

	// Keys
	RowNo()																									as Key_BestellArtikel,
	If(Len(Trim(Waehrung))=3,If(Upper(Trim(Waehrung))='RMB','CNY',Upper(Trim(Waehrung))),'EUR')				as Key_Iso4217_Alpha,
	Date(Floor(DatumStartArtikel),'YYYYMMDD')																as Key_Kalender_Datum,

	// Einheit
	If(IsNum(Preiseinheit),Preiseinheit,1)																	as tmp_PreisEinheit

FROM
	[$(vStdTaskQvdExtract)\Realcontrolling.BestellArtikel.qvd]
	(qvd)
;


// **************** ****************
// *** Data
// **************** ****************

Noconcatenate
Realcontrolling.BestellArtikel:
LOAD Distinct
	*,

	'A#'& Key_Kalender_Datum &'#'& Key_Iso4217_Alpha &'#'& Key_Abschlussbericht_Stamm						as Key_Umsatz
;
LOAD
	*,

	'A#'& Flag_TrackingArt &'#'& Abschlussbericht_Name &'#'& Key_WerkGesellschaft_Lager
		&'#'& If(Flag_Artikel=1,'A#'& Key_Artikel_Nr,'W#'& Key_Warengruppe)
		&'#L'& Key_Lieferant_Nr																				as Key_Abschlussbericht_Artikel,
	'A#'& Flag_TrackingArt
		&'#'& Key_WerkGesellschaft_Lager
		&'#'& If(Flag_Artikel=1,'A#'& Key_Artikel_Nr,'W#'& Key_Warengruppe)
		&'#L#'& Key_Lieferant_Nr																			as Key_Abschlussbericht_Stamm,
	Key_WerkGesellschaft &'#'& Key_Warengruppe																as Key_WerkGesellschaft_Warengruppe,
	Key_WerkGesellschaft_Lager &'#'& Key_Warengruppe														as Key_WerkGesellschaft_Lager_Warengruppe,
	If(Key_Warengruppe<>'$(vStdIniDescriptionUnknownId)',1,0)												as Flag_Warengruppe

;
LOAD
	*,

	// Keys
	If(Key_Warengruppe_Artikel<>'$(vStdIniDescriptionUnknownId)',Key_Warengruppe_Artikel
		,If(Flag_WarengruppeCode=1
			,ApplyMap('Map.Realcontrolling.BestellArtikel_Warengruppe_WerkGesellschaft',Key_WerkGesellschaft_Warengruppe_Code
			,ApplyMap('Map.Realcontrolling.BestellArtikel_Warengruppe_Allgemein',Key_Warengruppe_Code
			,'CD#'& Warengruppe_WarengruppeCode))
		,'$(vStdIniDescriptionUnknownId)'))																	as Key_Warengruppe

;
LOAD
	*,

	// Keys
	Key_WerkGesellschaft &'#'& Key_Artikel																	as Key_WerkGesellschaft_Artikel,
	Key_WerkGesellschaft_Lager &'#'& Key_Artikel															as Key_WerkGesellschaft_Lager_Artikel,
	If(Flag_WarengruppeCode=1,Key_WerkGesellschaft &'#'& Key_Warengruppe_Code)								as Key_WerkGesellschaft_Warengruppe_Code,
	Key_WerkGesellschaft &'#'& Key_Lieferant																as Key_WerkGesellschaft_Lieferant,
	Key_WerkGesellschaft_Lager &'#'& Key_Lieferant															as Key_WerkGesellschaft_Lager_Lieferant,
	ApplyMap('Map.Realcontrolling.BestellArtikel_Warengruppe',Key_Artikel
		,'$(vStdIniDescriptionUnknownId)')																	as Key_Warengruppe_Artikel,

	// Fields
	Num(BestellArtikel_PreisAlt_PreisEinheit*BestellArtikel_MengeEinheit_ReFaktor)							as BestellArtikel_PreisAlt_RealControlling,
	Num(BestellArtikel_PreisNeu_PreisEinheit*BestellArtikel_MengeEinheit_ReFaktor)							as BestellArtikel_PreisNeu_RealControlling,
	Num(BestellArtikel_PreisEinsparung_PreisEinheit*BestellArtikel_MengeEinheit_ReFaktor)					as BestellArtikel_PreisEinsparung_RealControlling,
	Num(BestellArtikel_Pauschale_PreisEinheit*BestellArtikel_MengeEinheit_ReFaktor)							as BestellArtikel_Pauschale_RealControlling

;
LOAD
	*,

	// Keys
	Key_WerkGesellschaft_Lager &'#'& Key_Artikel_Nr															as Key_WerkGesellschaft_Lager_Artikel_Nr,
	Key_WerkGesellschaft_Lager &'#'& Key_Lieferant_Nr														as Key_WerkGesellschaft_Lager_Lieferant_Nr,
	ApplyMap('Map.Realcontrolling.BestellArtikel_Artikel_WerkGesellschaft',Key_WerkGesellschaft_Artikel_Nr
		,ApplyMap('Map.Realcontrolling.BestellArtikel_Artikel_Artikel',Key_Artikel_Nr
		,If(Flag_Artikel=1,'NR#'& Key_Artikel_Nr,Key_Artikel_Nr)))											as Key_Artikel,
	ApplyMap('Map.Realcontrolling.BestellArtikel_Lieferant_Id',Key_Lieferant_Id
		,ApplyMap('Map.Realcontrolling.BestellArtikel_Lieferant',Key_WerkGesellschaft_Lieferant_Nr
		,If(Flag_Lieferant=1,'NR#'& Key_Lieferant_Nr,Key_Lieferant_Nr)))									as Key_Lieferant,

	// Fields
	Num(BestellArtikel_PreisAlt/BestellArtikel_PreisEinheit)												as BestellArtikel_PreisAlt_PreisEinheit,
	Num(BestellArtikel_PreisNeu/BestellArtikel_PreisEinheit)												as BestellArtikel_PreisNeu_PreisEinheit,
	Num(BestellArtikel_PreisEinsparung/BestellArtikel_PreisEinheit)											as BestellArtikel_PreisEinsparung_PreisEinheit,
	Num(BestellArtikel_Pauschale/BestellArtikel_PreisEinheit)												as BestellArtikel_Pauschale_PreisEinheit,
	Num(BestellArtikel_WaehrungReRate*BestellArtikel_EinsparungTag_Original)								as BestellArtikel_EinsparungTag

;
LOAD
	*,

	// Keys
	Key_WerkGesellschaft &'##'																				as Key_WerkGesellschaft_Lager,
	Key_WerkGesellschaft &'#'& Key_Artikel_Nr																as Key_WerkGesellschaft_Artikel_Nr,
	Key_WerkGesellschaft &'#'& Key_Lieferant_Nr																as Key_WerkGesellschaft_Lieferant_Nr,

	// Flag
	If(Len(Trim(Key_WerkGesellschaft))>3,1,0)																as Flag_WerkGesellschaft,

	// Fields
	Num(BestellArtikel_Menge
		/(BestellArtikel_DatumPlanungBis-BestellArtikel_DatumPlanungVon+1))									as BestellArtikel_MengeTag,
	Num(BestellArtikel_WaehrungReRate*BestellArtikel_PreisAlt_Original)										as BestellArtikel_PreisAlt,
	Num(BestellArtikel_WaehrungReRate*BestellArtikel_PreisNeu_Original)										as BestellArtikel_PreisNeu,
	Num(BestellArtikel_WaehrungReRate*BestellArtikel_PreisEinsparung_Original)								as BestellArtikel_PreisEinsparung,
	Num(BestellArtikel_WaehrungReRate*BestellArtikel_Einsparung_Original)									as BestellArtikel_Einsparung,
	Num(BestellArtikel_Einsparung_Original
		/(BestellArtikel_DatumPlanungBis-BestellArtikel_DatumPlanungVon+1))									as BestellArtikel_EinsparungTag_Original,
	Num(BestellArtikel_WaehrungReRate*BestellArtikel_Pauschale_Original)									as BestellArtikel_Pauschale,

;
LOAD
	*,

	// Keys
	'A#'& Abschlussbericht_Name &'#'& Key_BestellArtikel													as Key_Abschlussbericht,
	WerkGesellschaft_Gesellschaft
		&'#'& WerkGesellschaft_WerkStandort
		&'#'& WerkGesellschaft_WerkNr
		&'#'& WerkGesellschaft_WerkName																		as Key_WerkGesellschaft,
	ApplyMap('Map.Realcontrolling.BestellArtikel_Wechselkurs_Key',Key_Iso4217_Alpha_Datum,1)				as Key_Wechselkurs,

	// Flag
	If(Flag_TrackingArt='A','Anzahl'
		,If(Flag_TrackingArt='M','Menge'
		,'Volumen'))																						as Flag_TrackingArtAlias,

	BestellArtikel_DatumStartArtikel																		as BestellArtikel_DatumPlanungVon,
	If(IsNum(BestellArtikel_DatumEndeArtikel),BestellArtikel_DatumEndeArtikel
		,Date(Floor(AddMonths(BestellArtikel_DatumStartArtikel,BestellArtikel_Laufzeit)-1)))				as BestellArtikel_DatumPlanungBis,
	ApplyMap('Map.Realcontrolling.BestellArtikel_Wechselkurs_ReRate',Key_Iso4217_Alpha_Datum,1)				as BestellArtikel_WaehrungReRate,
	If(IsNum(BestellArtikel_DatumEndeArtikel),Num((BestellArtikel_MengeJahr/360)*(BestellArtikel_DatumEndeArtikel-BestellArtikel_DatumStartArtikel+1))
		,Num((BestellArtikel_MengeJahr/12)*BestellArtikel_Laufzeit))										as BestellArtikel_Menge,
	Num(ApplyMap('Map.Realcontrolling.BestellArtikel_MengenEinheit',Upper(Trim(BestellArtikel_MengenEinheit))
		,1))																								as BestellArtikel_MengeEinheit_ReFaktor,
	If(Flag_TrackingPreisNeu=1
		,Num(BestellArtikel_PreisAlt_Original-BestellArtikel_PreisNeu_Original))							as BestellArtikel_PreisEinsparung_Original

;
LOAD

	// Keys
	Key_BestellArtikel,
	Key_Kalender_Datum,
	Key_Iso4217_Alpha,
	Key_Iso4217_Alpha &'#'& Key_Kalender_Datum																as Key_Iso4217_Alpha_Datum,
	Text(ArtikelNr)																							as Key_Artikel_Nr,
	If(Len(PurgeChar(Warengruppe_Code,' ����.-'))>0,Text(Warengruppe_Code)
		,'$(vStdIniDescriptionUnknownId)')																	as Key_Warengruppe_Code,
	If(Len(PurgeChar(LieferantenNr,' ����.-'))>0,Text(LieferantenNr),'$(vStdIniDescriptionUnknownId)')		as Key_Lieferant_Nr,
	If(Len(PurgeChar(LieferantenID,' ����.-'))>0,'ID#'& Text(LieferantenID))								as Key_Lieferant_Id,

	// Flag
	If(Len(PurgeChar(ArtikelNr,' ����.-'))>0,1,0)															as Flag_Artikel,
	If(Len(PurgeChar(Warengruppe_Code,' ����.-'))>0,1,0)													as Flag_WarengruppeCode,
	If(Len(PurgeChar(LieferantenNr,' ����.-'))>0,1,0)														as Flag_Lieferant,
	If(Lower(Trim(ArtEinsparung))='anzahl','A'
		,If(Lower(Trim(ArtEinsparung))='menge','M'
		,'V'))																								as Flag_TrackingArt,

	If(Match(Upper(Trim(SecondSource)),'Y','X','J'),'Y','N')												as Flag_SecondSource,
	If(Match(Upper(Trim(LaufzeitFixiert)),'Y','X','J') or IsNum(DatumEndeArtikel),'Y','N')					as Flag_LaufzeitFixiert,
	If(IsNum(Preis_Neu) and Preis_Neu<>0,1,0)																as Flag_TrackingPreisNeu,
	If(EinsparungProJahr>0,1,0)																				as Flag_EinsparungPositiv,

	// WerkGesellschaft
	If(Len(PurgeChar(Gesellschaft,' ����.-'))>0,Text(Gesellschaft))											as WerkGesellschaft_Gesellschaft,
	If(Len(PurgeChar(WerkStandort,' ����.-'))>0,Text(WerkStandort))											as WerkGesellschaft_WerkStandort,
	If(Len(PurgeChar(WerkNr,' ����.-'))>0,Text(WerkNr))														as WerkGesellschaft_WerkNr,
	If(Len(PurgeChar(WerkName,' ����.-'))>0,Text(WerkName))													as WerkGesellschaft_WerkName,
	Null()																									as WerkGesellschaft_LagerNr,
	Null()																									as WerkGesellschaft_LagerName,

	// Warengruppe
	If(Len(PurgeChar(Warengruppe_Code,' ����.-'))>0,Text(Warengruppe_Code))									as Warengruppe_WarengruppeCode,

	// Abschlussbericht
	Abschlussbericht																						as Abschlussbericht_Name,

	// BestellArtikel
	Key_BestellArtikel																						as BestellArtikel_Id,
	If(Len(Trim(DatumEndeArtikel))=0,Num(Laufzeit))															as BestellArtikel_Laufzeit,
	Date(Floor(Key_Kalender_Datum))																			as BestellArtikel_DatumStartArtikel,
	If(IsNum(DatumEndeArtikel),Date(Floor(DatumEndeArtikel)))												as BestellArtikel_DatumEndeArtikel,
	Waehrung																								as BestellArtikel_Waehrung,
	If(Len(PurgeChar(Kommentar,' ����.-'))>0,Kommentar)														as BestellArtikel_Kommentar,
	If(Len(PurgeChar(Quelle,' ����.-'))>0,Quelle)															as BestellArtikel_Quelle,

	// Menge
	Num(If(IsNum(MengeProJahr),Num(MengeProJahr),1))														as BestellArtikel_MengeJahr,
	Mengeneinheit																							as BestellArtikel_MengenEinheit,

	// Preis
	Num(tmp_PreisEinheit)																					as BestellArtikel_PreisEinheit,
	Num(Preis_Alt)																							as BestellArtikel_PreisAlt_Original,
	Num(Preis_Neu)																							as BestellArtikel_PreisNeu_Original,
	If(Left(Upper(Trim(ArtEinsparung)),1)='M',Num(Pauschale))												as BestellArtikel_Pauschale_Original,
	Num((EinsparungProJahr/12)*Laufzeit)																	as BestellArtikel_Einsparung_Original

Resident
	Realcontrolling.BestellArtikel_tmp
Where
		Len(PurgeChar(ArtikelNr,' ����.-'))>0
	and
		(Match(Upper(Trim(SecondSource)),'Y','X','J') or Len(PurgeChar(LieferantenNr,' ����.-'))>0)
	and
		(IsNum(Key_Kalender_Datum) and (IsNum(Laufzeit) or IsNum(DatumEndeArtikel)))
	and
		(((Len(Trim(ArtEinsparung))=0 or Left(Upper(Trim(ArtEinsparung)),1)='V') and IsNum(Preis_Alt)) or (Left(Upper(Trim(ArtEinsparung)),1)='M' and IsNum(Pauschale)))
;


Left Join
(Realcontrolling.BestellArtikel)
LOAD
	Key_Abschlussbericht_Stamm,
	Count(DISTINCT BestellArtikel_DatumPlanungVon)															as BestellArtikel_LaufzeitAnzahl,
	Floor(Max(BestellArtikel_DatumPlanungBis))-Floor(Min(BestellArtikel_DatumPlanungVon))+1					as BestellArtikel_LaufzeitTage
Resident
	Realcontrolling.BestellArtikel
Group By
	Key_Abschlussbericht_Stamm
;


// **************** ****************
// *** Error / Clear
// **************** ****************

CALL subLogError;
CALL subCommonDropTable('Realcontrolling.BestellArtikel_tmp');



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: subValue;
TRACE **************** **************** **************** ****************;

CALL subValueIso4217;



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Wechselkurs;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.BestellArtikel_Wechselkurs:
LOAD Distinct
	Key_Wechselkurs,
	Key_Iso4217_Alpha_Datum,
	Key_Iso4217_Alpha,
	Key_Kalender_Datum,
	Wechselkurs_Rate,
	Wechselkurs_ReRate
FROM
[$(vStdTaskQvdTransact)\Einkaufstracker.Wechselkurs.qvd]
(qvd)
Where
		Flag_Gueltig=1
	and
		Match(Key_Iso4217_Alpha,$(vCalcWaehrungRate))
;


FOR Each vBestellArtikelWaehrung in $(vCalcWaehrungRate)

	NoConcatenate
	[Realcontrolling.BestellArtikel_Wechselkurs_$(vBestellArtikelWaehrung)]:
	LOAD Distinct
		*
	Resident
		Realcontrolling.BestellArtikel_Wechselkurs
	Where
		Key_Iso4217_Alpha='$(vBestellArtikelWaehrung)'
	;


	Left Join
	([Realcontrolling.BestellArtikel_Wechselkurs_$(vBestellArtikelWaehrung)])
	LOAD Distinct
		Key_Kalender_Datum,
		Key_Abschlussbericht,
		Key_Iso4217_Alpha																					as tmp_Key_Iso4217_Alpha,
		BestellArtikel_PreisAlt,
		BestellArtikel_PreisAlt_PreisEinheit,
		BestellArtikel_PreisAlt_Original,
		BestellArtikel_PreisNeu,
		BestellArtikel_PreisNeu_PreisEinheit,
		BestellArtikel_PreisNeu_Original,
		BestellArtikel_PreisEinsparung,
		BestellArtikel_PreisEinsparung_PreisEinheit,
		BestellArtikel_PreisEinsparung_Original,
		BestellArtikel_Einsparung,
		BestellArtikel_Einsparung_Original,
		BestellArtikel_EinsparungTag,
		BestellArtikel_EinsparungTag_Original,
		BestellArtikel_Pauschale,
		BestellArtikel_Pauschale_PreisEinheit,
		BestellArtikel_Pauschale_Original
	Resident
		Realcontrolling.BestellArtikel
	;


	Left Join
	(Realcontrolling.BestellArtikel)
	LOAD Distinct
		Key_Abschlussbericht,

		// BestellArtikel
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,BestellArtikel_PreisAlt_Original
			,Wechselkurs_Rate*BestellArtikel_PreisAlt),$(vStdFormatNumberDec))								as [BestellArtikel_PreisAlt_$(vBestellArtikelWaehrung)],
		Num(Wechselkurs_Rate*BestellArtikel_PreisAlt_PreisEinheit,$(vStdFormatNumberDec))					as [BestellArtikel_PreisAlt_PreisEinheit_$(vBestellArtikelWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,BestellArtikel_PreisNeu_Original
			,Wechselkurs_Rate*BestellArtikel_PreisNeu),$(vStdFormatNumberDec))								as [BestellArtikel_PreisNeu_$(vBestellArtikelWaehrung)],
		Num(Wechselkurs_Rate*BestellArtikel_PreisNeu_PreisEinheit,$(vStdFormatNumberDec))					as [BestellArtikel_PreisNeu_PreisEinheit_$(vBestellArtikelWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,BestellArtikel_PreisEinsparung_Original
			,Wechselkurs_Rate*BestellArtikel_PreisEinsparung),$(vStdFormatNumberDec))						as [BestellArtikel_PreisEinsparung_$(vBestellArtikelWaehrung)],
		Num(Wechselkurs_Rate*BestellArtikel_PreisEinsparung_PreisEinheit,$(vStdFormatNumberDec))			as [BestellArtikel_PreisEinsparung_PreisEinheit_$(vBestellArtikelWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,BestellArtikel_Einsparung_Original
			,Wechselkurs_Rate*BestellArtikel_Einsparung),$(vStdFormatNumberDec))							as [BestellArtikel_Einsparung_$(vBestellArtikelWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,BestellArtikel_EinsparungTag_Original
			,Wechselkurs_Rate*BestellArtikel_EinsparungTag),$(vStdFormatNumberDec))							as [BestellArtikel_EinsparungTag_$(vBestellArtikelWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,BestellArtikel_Pauschale_Original
			,Wechselkurs_Rate*BestellArtikel_Pauschale),$(vStdFormatNumberDec))								as [BestellArtikel_Pauschale_$(vBestellArtikelWaehrung)],
		Num(Wechselkurs_Rate*BestellArtikel_Pauschale_PreisEinheit,$(vStdFormatNumberDec))					as [BestellArtikel_Pauschale_PreisEinheit_$(vBestellArtikelWaehrung)]

	Resident
		[Realcontrolling.BestellArtikel_Wechselkurs_$(vBestellArtikelWaehrung)]
	;


	// Clear
	CALL subCommonDropTable('Realcontrolling.BestellArtikel_Wechselkurs_$(vBestellArtikelWaehrung)');

NEXT vBestellArtikelWaehrung


// Clear
CALL subCommonDropTable('Realcontrolling.BestellArtikel_Wechselkurs');
LET vBestellArtikelWaehrung		= Null();



TRACE **************** **************** **************** ****************;
TRACE *** BestellArtikel: Tracking;
TRACE **************** **************** **************** ****************;

IF FileSize('$(vStdTaskQvdTransact)\Einkaufstracker.Bestellung.qvd')>0 then

	NoConcatenate
	Realcontrolling.BestellArtikel_Tracking:
	LOAD Distinct
		Key_Abschlussbericht,
//		Key_WerkGesellschaft,
		Key_Artikel_Nr,
//		Flag_WerkGesellschaft,
		BestellArtikel_DatumStartArtikel
	Resident
		Realcontrolling.BestellArtikel
	Where
		Flag_Artikel=1
	;


	NoConcatenate
	Realcontrolling.BestellArtikel_Bestellung:
	LOAD
//		Key_WerkGesellschaft,
		Key_Artikel_Nr,
		Bestellung_DatumBestellung
	FROM
		[$(vStdTaskQvdTransact)\Einkaufstracker.Bestellung.qvd]
		(qvd)
	;


	Left Join
	(Realcontrolling.BestellArtikel_Tracking)
	LOAD Distinct
		Key_Artikel_Nr,
		0																									as Flag_WerkGesellschaft,
		Bestellung_DatumBestellung																			as Bestellung_DatumBestellung_Gesamt
	Resident
		Realcontrolling.BestellArtikel_Bestellung
	;

	Left Join
	(Realcontrolling.BestellArtikel_Tracking)
	LOAD Distinct
		Key_Abschlussbericht,
		Date(Floor(Min(Bestellung_DatumBestellung_Gesamt)))													as BestellArtikel_DatumStartTracking_Gesamt
	Resident
		Realcontrolling.BestellArtikel_Tracking
	Where
			Bestellung_DatumBestellung_Gesamt>=BestellArtikel_DatumStartArtikel
	Group By
		Key_Abschlussbericht
	;


//	Left Join
//	(Realcontrolling.BestellArtikel_Tracking)
//	LOAD Distinct
//		Key_WerkGesellschaft,
//		Key_Artikel_Nr,
//		Bestellung_DatumBestellung																			as Bestellung_DatumBestellung_WerkGesellschaft
//	Resident
//		Realcontrolling.BestellArtikel_Bestellung
//	;
//
//	Left Join
//	(Realcontrolling.BestellArtikel_Tracking)
//	LOAD Distinct
//		Key_Abschlussbericht,
//		Date(Floor(Min(Bestellung_DatumBestellung_WerkGesellschaft)))										as BestellArtikel_DatumStartTracking_WerkGesellschaft
//	Resident
//		Realcontrolling.BestellArtikel_Tracking
//	Where
//			Bestellung_DatumBestellung_WerkGesellschaft>=BestellArtikel_DatumStartArtikel
//	Group By
//		Key_Abschlussbericht
//	;


	Left Join
	(Realcontrolling.BestellArtikel)
	LOAD Distinct
		Key_Abschlussbericht,
		BestellArtikel_DatumStartTracking_Gesamt															as BestellArtikel_DatumStartTracking
//		If(Flag_WerkGesellschaft=0,BestellArtikel_DatumStartTracking_Gesamt
//			,BestellArtikel_DatumStartTracking_WerkGesellschaft)											as BestellArtikel_DatumStartTracking
	Resident
		Realcontrolling.BestellArtikel_Tracking
	;


	// Clear
	CALL subCommonDropTable('Realcontrolling.BestellArtikel_Tracking');
	CALL subCommonDropTable('Realcontrolling.BestellArtikel_Bestellung');

ELSE

	Concatenate
	(Realcontrolling.BestellArtikel)
	LOAD * Inline [
		BestellArtikel_DatumStartTracking
	];

ENDIF


Left Join
(Realcontrolling.BestellArtikel)
LOAD Distinct
	Key_Abschlussbericht,
	If(Len(BestellArtikel_DatumStartTracking)>0,'Y','N')													as Flag_Tracking
Resident
	Realcontrolling.BestellArtikel
;

Left Join
(Realcontrolling.BestellArtikel)
LOAD Distinct
	Key_Abschlussbericht,
	If(Flag_LaufzeitFixiert='Y',BestellArtikel_DatumPlanungVon,BestellArtikel_DatumStartTracking)			as BestellArtikel_DatumTrackingVon,
	If(Flag_LaufzeitFixiert='Y',BestellArtikel_DatumPlanungBis
		,Date(Floor(Num(BestellArtikel_DatumStartTracking)+BestellArtikel_LaufzeitTage)))					as BestellArtikel_DatumTrackingBis
Resident
	Realcontrolling.BestellArtikel
Where
		Flag_Tracking='Y'
;

