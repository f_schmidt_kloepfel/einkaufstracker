// ******** TRANSACT - Realcontrolling.Manuell ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-02-27	1.00		created
// 2018-03-11	1.10		modified
// 2018-04-04	1.20		modified
// 2018-04-05	1.30		modified
// 2018-04-09	1.31		mod Fields: Manuell_Realisiert*; add Fields: Flag_TrackingArt*
// 2018-04-09	1.32		mod: Error
// 2018-04-20	1.40		mod: Pauschale
// 2018-04-24	1.41		mod: Pauschale
// 2018-05-07	1.50		mod: Preiseinheit
// 2018-05-18	1.51		mod: Pauschale
// 2018-07-06	1.60		mod Fields: Len(PurgeChar())
// 2018-07-17	1.61		mod Fields: Len(PurgeChar())
//
// ******** TRANSACT - Realcontrolling.Manuell ********



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Mapping;
TRACE **************** **************** **************** ****************;

CALL subMappingWaehrungDate;
CALL subMappingArtikelWarengruppe;
CALL subMappingWarengruppeStamm;
CALL subMappingLieferant;



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Extract;
TRACE **************** **************** **************** ****************;


// **************** ****************
// *** Key
// **************** ****************

NoConcatenate
Realcontrolling.Manuell_tmp:
LOAD
	*,

	// Keys
	RowNo()																							as Key_Manuell,
	If(Len(Trim(Waehrung))=3,If(Upper(Trim(Waehrung))='RMB','CNY',Upper(Trim(Waehrung))),'EUR')		as Key_Iso4217_Alpha,
	If(Len(Trim(DatumStart)) and IsNum(DatumStart),Date(Floor(DatumStart),'YYYYMMDD'))				as Key_Kalender_Datum,

	// Einheit
	If(IsNum(Preiseinheit),Preiseinheit,1)															as tmp_PreisEinheit,

	// Tracking
	If(Match(Upper(Left(Trim(Frequenz_Intervall),1)),'Y','Q','M','W') and Match(Upper(Left(Trim(Frequenz_AnfangEnde),1)),'A','E')
		,'P','V')																					as Flag_TrackingArt

FROM
	[$(vStdTaskQvdExtract)\Realcontrolling.Manuell.qvd]
	(qvd)
;


// **************** ****************
// *** Data
// **************** ****************

Noconcatenate
Realcontrolling.Manuell_pre:
LOAD Distinct
	*,

	'M#'& Key_Kalender_Datum &'#'& Key_Iso4217_Alpha &'#'& Key_Stamm								as Key_Umsatz

;
LOAD
	*,

	// Keys
	If(Flag_Artikel=1,'A#'& Key_Artikel_Nr,'W#'& Key_Warengruppe)
		&'#L#'& Key_Lieferant_Nr &'#'& Key_WerkGesellschaft_Lager									as Key_Stamm,
	Key_WerkGesellschaft &'#'& Key_Warengruppe														as Key_WerkGesellschaft_Warengruppe,
	Key_WerkGesellschaft_Lager &'#'& Key_Warengruppe												as Key_WerkGesellschaft_Lager_Warengruppe,

	// Flag
	If(Key_Warengruppe<>'$(vStdIniDescriptionUnknownId)',1,0)										as Flag_Warengruppe

;
LOAD
	*,

	// Keys
	Key_WerkGesellschaft &'#'& Key_Artikel															as Key_WerkGesellschaft_Artikel,
	Key_WerkGesellschaft_Lager &'#'& Key_Artikel													as Key_WerkGesellschaft_Lager_Artikel,
	Key_WerkGesellschaft &'#'& Key_Lieferant														as Key_WerkGesellschaft_Lieferant,
	Key_WerkGesellschaft_Lager &'#'& Key_Lieferant													as Key_WerkGesellschaft_Lager_Lieferant,
	ApplyMap('Map.Realcontrolling.Manuell_Warengruppe',Key_Artikel
		,'$(vStdIniDescriptionUnknownId)')															as Key_Warengruppe

;
LOAD
	*,

	// Keys
	Key_WerkGesellschaft_Lager &'#'& Key_Artikel_Nr													as Key_WerkGesellschaft_Lager_Artikel_Nr,
	Key_WerkGesellschaft_Lager &'#'& Key_Lieferant_Nr												as Key_WerkGesellschaft_Lager_Lieferant_Nr,
	ApplyMap('Map.Realcontrolling.Manuell_Artikel_WerkGesellschaft',Key_WerkGesellschaft_Artikel_Nr
		,ApplyMap('Map.Realcontrolling.Manuell_Artikel_Artikel',Key_Artikel_Nr
		,If(Flag_Artikel=1,'NR#'& Key_Artikel_Nr,Key_Artikel_Nr)))									as Key_Artikel,
	ApplyMap('Map.Realcontrolling.Manuell_Lieferant',Key_WerkGesellschaft_Lieferant_Nr
		,If(Flag_Lieferant=1,'NR#'& Key_Lieferant_Nr,Key_Lieferant_Nr))								as Key_Lieferant,

	// Fields
	Num(Manuell_PreisAlt/Manuell_PreisEinheit)														as Manuell_PreisAlt_PreisEinheit,
	Num(Manuell_PreisNeu/Manuell_PreisEinheit)														as Manuell_PreisNeu_PreisEinheit,
	Num(Manuell_PreisEinsparung/Manuell_PreisEinheit)												as Manuell_PreisEinsparung_PreisEinheit,
	Num(Manuell_WaehrungReRate*Manuell_Planung_EinsparungTag_Original)								as Manuell_Planung_EinsparungTag,
	Num(Manuell_WaehrungReRate_Realisiert*Manuell_Realisiert_Einsparung_Original)					as Manuell_Realisiert_Einsparung

;
LOAD
	*,

	// Keys
	Key_WerkGesellschaft &'##'																		as Key_WerkGesellschaft_Lager,
	Key_WerkGesellschaft &'#'& Key_Artikel_Nr														as Key_WerkGesellschaft_Artikel_Nr,
	Key_WerkGesellschaft &'#'& Key_Lieferant_Nr														as Key_WerkGesellschaft_Lieferant_Nr,
	If(Flag_TrackingArt='P',Key_Iso4217_Alpha &'#'
			& If(Manuell_Pauschale_AnfangEnde='A',Date(Manuell_DatumManuellVon,'YYYYMMDD'),Date(Manuell_DatumManuellBis,'YYYYMMDD'))
	)																								as Key_Iso4217_Alpha_Datum_Pauschale,

	// Flag
	If(Len(Trim(Key_WerkGesellschaft))>3,1,0)														as Flag_WerkGesellschaft,
	If(fabs(Manuell_Realisiert_Einsparung_Original)>0 and IsNum(Manuell_DatumRealisiert),'Y','N')	as Flag_EinsparungRealisiert,

	// Fields
	ApplyMap('Map.Realcontrolling.Manuell_Wechselkurs_ReRate',Key_Iso4217_Alpha_Datum_Realisiert,1)	as Manuell_WaehrungReRate_Realisiert,

	// Menge
	Num(tmp_Manuell_Planung_Menge
		/(Manuell_DatumManuellBis-Manuell_DatumManuellVon+1))										as tmp_Manuell_Planung_MengeTag,
	// Preis
	Num(Manuell_WaehrungReRate*Manuell_PreisAlt_Original)											as Manuell_PreisAlt,
	Num(Manuell_WaehrungReRate*Manuell_PreisNeu_Original)											as Manuell_PreisNeu,
	Num(Manuell_WaehrungReRate*Manuell_PreisEinsparung_Original)									as Manuell_PreisEinsparung,
	// Einsparung
	Num(Manuell_WaehrungReRate*Manuell_Planung_Einsparung_Original)									as Manuell_Planung_Einsparung,
	Num(Manuell_Planung_Einsparung_Original
		/(Manuell_DatumManuellBis-Manuell_DatumManuellVon+1))										as Manuell_Planung_EinsparungTag_Original,

	// Pauschale
	Num(Manuell_WaehrungReRate*Manuell_Pauschale_Einsparung_Original)								as Manuell_Pauschale_Einsparung

;
LOAD

	// Keys
	Key_Iso4217_Alpha &'#'& Date(Manuell_DatumRealisiert,'YYYYMMDD')								as Key_Iso4217_Alpha_Datum_Realisiert,
	'M#'& Abschlussbericht_Name &'#'& Key_Manuell													as Key_Abschlussbericht,
	WerkGesellschaft_Gesellschaft
		&'#'& WerkGesellschaft_WerkStandort
		&'#'& WerkGesellschaft_WerkNr
		&'#'& WerkGesellschaft_WerkName																as Key_WerkGesellschaft,
	ApplyMap('Map.Realcontrolling.Manuell_Wechselkurs_Key',Key_Iso4217_Alpha_Datum,1)				as Key_Wechselkurs,
	ApplyMap('Map.Realcontrolling.Manuell_Wechselkurs_Key',Key_Iso4217_Alpha_Datum,1)				as Key_Wechselkurs_Realisiert,

	*,
	ApplyMap('Map.Realcontrolling.Manuell_Wechselkurs_ReRate',Key_Iso4217_Alpha_Datum,1)			as Manuell_WaehrungReRate,
	Num(Manuell_PreisAlt_Original-Manuell_PreisNeu_Original)										as Manuell_PreisEinsparung_Original

;
LOAD

	// Keys
	Key_Manuell,
	Key_Kalender_Datum,
	Key_Iso4217_Alpha,
	Key_Iso4217_Alpha &'#'& Key_Kalender_Datum														as Key_Iso4217_Alpha_Datum,
	If(Len(PurgeChar(ArtikelNr,' ·–…—.-'))>0,Text(ArtikelNr),'$(vStdIniDescriptionUnknownId)')		as Key_Artikel_Nr,
	Null()																							as Key_Warengruppe_Code,
	If(Len(PurgeChar(LieferantenNr,' ·–…—.-'))>0,Text(LieferantenNr)
		,'$(vStdIniDescriptionUnknownId)')															as Key_Lieferant_Nr,

	// Flag
	If(Len(PurgeChar(ArtikelNr,' ·–…—.-'))>0,1,0)													as Flag_Artikel,
	0																								as Flag_WarengruppeCode,
	If(Len(PurgeChar(LieferantenNr,' ·–…—.-'))>0,1,0)												as Flag_Lieferant,
	Flag_TrackingArt,
	If(Flag_TrackingArt='P','Pauschale','Volumen')													as Flag_TrackingArtAlias,

	// WerkGesellschaft
	If(Len(PurgeChar(Gesellschaft,' ·–…—.-'))>0,Text(Gesellschaft))									as WerkGesellschaft_Gesellschaft,
	If(Len(PurgeChar(WerkStandort,' ·–…—.-'))>0,Text(WerkStandort))									as WerkGesellschaft_WerkStandort,
	If(Len(PurgeChar(WerkNr,' ·–…—.-'))>0,Text(WerkNr))												as WerkGesellschaft_WerkNr,
	If(Len(PurgeChar(WerkName,' ·–…—.-'))>0,Text(WerkName))											as WerkGesellschaft_WerkName,
	Null()																							as WerkGesellschaft_LagerNr,
	Null()																							as WerkGesellschaft_LagerName,

	// Warengruppe
	Null()																							as Warengruppe_WarengruppeCode,

	// Abschlussbericht
	Abschlussbericht																				as Abschlussbericht_Name,

	// Manuell
	Key_Manuell																						as Manuell_Id,
	Date(Floor(DatumStart))																			as Manuell_DatumManuellVon,
	Date(Floor(DatumEnde))																			as Manuell_DatumManuellBis,
	Date(Floor(DatumRealisiert))																	as Manuell_DatumRealisiert,
	Waehrung																						as Manuell_Waehrung,
	If(Len(PurgeChar(Kommentar,' ·–…—.-'))>0,Kommentar)												as Manuell_Kommentar,
	If(Len(PurgeChar(Quelle,' ·–…—.-'))>0,Quelle)													as Manuell_Quelle,

	// Menge
	Mengeneinheit																					as Manuell_MengenEinheit,
	Num(MengePlan)																					as tmp_Manuell_Planung_Menge,
	If(Len(Trim(Frequenz_Intervall))=0,Num(MengeRealisiert))										as Manuell_Realisiert_Menge,

	// Preis
	Num(tmp_PreisEinheit)																			as Manuell_PreisEinheit,
	Num(Preis_Alt/tmp_PreisEinheit)																	as Manuell_PreisAlt_Original,
	Num(Preis_Neu/tmp_PreisEinheit)																	as Manuell_PreisNeu_Original,

	// Einsparung
	Num(EinsparungPlan)																				as Manuell_Planung_Einsparung_Original,
	If(Len(Trim(Frequenz_Intervall))=0,Num(EinsparungRealisiert))									as Manuell_Realisiert_Einsparung_Original,

	// Pauschale
	Upper(Left(Trim(Frequenz_Intervall),1))															as Manuell_Pauschale_Intervall,
	Upper(Left(Trim(Frequenz_AnfangEnde),1))														as Manuell_Pauschale_AnfangEnde,
	If(Len(Trim(Frequenz_Intervall))>0,Num(Pauschale/tmp_PreisEinheit))								as Manuell_Pauschale_Einsparung_Original

Resident
	Realcontrolling.Manuell_tmp
Where
		IsNum(Key_Kalender_Datum) and IsNum(DatumEnde)
	and
		(Len(Trim(Frequenz_Intervall))=0
		or (IsNum(DatumEnde) and IsNum(Pauschale) and Match(Upper(Left(Trim(Frequenz_Intervall),1)),'Y','J','Q','M','W') and Match(Upper(Left(Trim(Frequenz_AnfangEnde),1)),'A','E')))
;


// **************** ****************
// *** Error / Clear
// **************** ****************

CALL subLogError;
CALL subCommonDropTable('Realcontrolling.Manuell_tmp');



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Pauschale;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.Manuell_Pauschale:
LOAD
	Key_Manuell,
	Flag_TrackingArt,
	Manuell_Pauschale_Intervall,
	Manuell_Pauschale_AnfangEnde,
	Manuell_DatumManuellVon,
	Manuell_DatumManuellBis
Resident
Realcontrolling.Manuell_pre
Where	Flag_TrackingArt='P'
;


NoConcatenate
Realcontrolling.Manuell_Kalender:
LOAD Distinct
	*
FROM
[$(vStdTaskQvdTransact)\KC.Kalender.qvd]
(qvd)
;

Inner Join
IntervalMatch
(Kalender_Datum)
LOAD Distinct
	Manuell_DatumManuellVon,
	Manuell_DatumManuellBis
Resident
Realcontrolling.Manuell_Pauschale
;

Left Join
(Realcontrolling.Manuell_Pauschale)
LOAD Distinct
	Manuell_DatumManuellVon,
	Manuell_DatumManuellBis,
	Kalender_Datum,
	Kalender_StartWoche,
	Kalender_StartMonat,
	Kalender_StartQuartal,
	Kalender_StartJahr,
	Kalender_EndeWoche,
	Kalender_EndeMonat,
	Kalender_EndeQuartal,
	Kalender_EndeJahr
Resident
Realcontrolling.Manuell_Kalender
;

Left Join
(Realcontrolling.Manuell_pre)
LOAD
	Key_Manuell,
	Manuell_Pauschale_Intervall,
	Manuell_Pauschale_AnfangEnde,
	If(Manuell_Pauschale_Intervall='Y'
			,If(Manuell_Pauschale_AnfangEnde='A',Count(Distinct If(Floor(Kalender_StartJahr)=Floor(Kalender_Datum),Kalender_StartJahr))
				,Count(Distinct If(Floor(Kalender_EndeJahr)=Floor(Kalender_Datum),Kalender_EndeJahr)))
		,If(Manuell_Pauschale_Intervall='Q'
			,If(Manuell_Pauschale_AnfangEnde='A',Count(Distinct If(Floor(Kalender_StartQuartal)=Floor(Kalender_Datum),Kalender_StartQuartal))
				,Count(Distinct If(Floor(Kalender_EndeQuartal)=Floor(Kalender_Datum),Kalender_EndeQuartal)))
		,If(Manuell_Pauschale_Intervall='M'
			,If(Manuell_Pauschale_AnfangEnde='A',Count(Distinct If(Floor(Kalender_StartMonat)=Floor(Kalender_Datum),Kalender_StartMonat))
				,Count(Distinct If(Floor(Kalender_EndeMonat)=Floor(Kalender_Datum),Kalender_EndeMonat)))
		,If(Manuell_Pauschale_Intervall='W'
			,If(Manuell_Pauschale_AnfangEnde='A',Count(Distinct If(Floor(Kalender_StartWoche)=Floor(Kalender_Datum),Kalender_StartWoche))
				,Count(Distinct If(Floor(Kalender_EndeWoche)=Floor(Kalender_Datum),Kalender_EndeWoche)))
		))))																					as Manuell_Pauschale_Menge
Resident
Realcontrolling.Manuell_Pauschale
Group By
	Key_Manuell
	,Manuell_Pauschale_Intervall
	,Manuell_Pauschale_AnfangEnde
;


Left Join
(Realcontrolling.Manuell_pre)
LOAD Distinct
	Key_Manuell,
	Manuell_Pauschale_AnfangEnde,
	If(Manuell_Pauschale_Intervall='Y',Date(Floor(Kalender_StartJahr))
		,If(Manuell_Pauschale_Intervall='Q',Date(Floor(Kalender_StartQuartal))
		,If(Manuell_Pauschale_Intervall='M',Date(Floor(Kalender_StartMonat))
		,If(Manuell_Pauschale_Intervall='W',Date(Floor(Kalender_StartWoche))
		))))																					as Manuell_Pauschale_GueltigVon,
	If(Manuell_Pauschale_Intervall='Y',Date(Floor(Kalender_EndeJahr))
		,If(Manuell_Pauschale_Intervall='Q',Date(Floor(Kalender_EndeQuartal))
		,If(Manuell_Pauschale_Intervall='M',Date(Floor(Kalender_EndeMonat))
		,If(Manuell_Pauschale_Intervall='W',Date(Floor(Kalender_EndeWoche))
		))))																					as Manuell_Pauschale_GueltigBis
Resident
	Realcontrolling.Manuell_Pauschale
Where
		(Manuell_Pauschale_Intervall='Y' and
			((Manuell_Pauschale_AnfangEnde='A' and Manuell_DatumManuellVon<=Floor(Kalender_StartJahr) and Manuell_DatumManuellBis>=Floor(Kalender_StartJahr))
			or (Manuell_Pauschale_AnfangEnde='E' and Manuell_DatumManuellVon<=Floor(Kalender_EndeJahr) and Manuell_DatumManuellBis>=Floor(Kalender_EndeJahr))))
	or
		(Manuell_Pauschale_Intervall='Q' and
			((Manuell_Pauschale_AnfangEnde='A' and Manuell_DatumManuellVon<=Floor(Kalender_StartQuartal) and Manuell_DatumManuellBis>=Floor(Kalender_StartQuartal))
			or (Manuell_Pauschale_AnfangEnde='E' and Manuell_DatumManuellVon<=Floor(Kalender_EndeQuartal) and Manuell_DatumManuellBis>=Floor(Kalender_EndeQuartal))))
	or
		(Manuell_Pauschale_Intervall='M' and
			((Manuell_Pauschale_AnfangEnde='A' and Manuell_DatumManuellVon<=Floor(Kalender_StartMonat) and Manuell_DatumManuellBis>=Floor(Kalender_StartMonat))
			or (Manuell_Pauschale_AnfangEnde='E' and Manuell_DatumManuellVon<=Floor(Kalender_EndeMonat) and Manuell_DatumManuellBis>=Floor(Kalender_EndeMonat))))
	or
		(Manuell_Pauschale_Intervall='W' and
			((Manuell_Pauschale_AnfangEnde='A' and Manuell_DatumManuellVon<=Floor(Kalender_StartWoche) and Manuell_DatumManuellBis>=Floor(Kalender_StartWoche))
			or (Manuell_Pauschale_AnfangEnde='E' and Manuell_DatumManuellVon<=Floor(Kalender_EndeWoche) and Manuell_DatumManuellBis>=Floor(Kalender_EndeWoche))))
;


// Clear
CALL subCommonDropTable('Realcontrolling.Manuell_Kalender');
CALL subCommonDropTable('Realcontrolling.Manuell_Pauschale');



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Final;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.Manuell:
LOAD Distinct
	*,

	If(not IsNull(tmp_Manuell_Planung_Menge),tmp_Manuell_Planung_Menge
		,If(Flag_TrackingArt='P',Manuell_Pauschale_Menge))										as Manuell_Planung_Menge,
	If(not IsNull(tmp_Manuell_Planung_MengeTag),tmp_Manuell_Planung_MengeTag
		,If(Flag_TrackingArt='P',Num(Manuell_Pauschale_Menge/(Manuell_DatumManuellBis-Manuell_DatumManuellVon+1)))
		)																						as Manuell_Planung_MengeTag,

	If(Flag_TrackingArt='P'
		,If(Manuell_Pauschale_AnfangEnde='A',Date(Manuell_Pauschale_GueltigVon)
		,Date(Manuell_Pauschale_GueltigBis)))													as Manuell_Pauschale_GueltigRealisiert

Resident
Realcontrolling.Manuell_pre
;


// Clear
CALL subCommonDropTable('Realcontrolling.Manuell_pre');
DROP Fields
	tmp_Manuell_Planung_Menge
	,tmp_Manuell_Planung_MengeTag
;
  


TRACE **************** **************** **************** ****************;
TRACE *** Manuell: subValue;
TRACE **************** **************** **************** ****************;

CALL subValueIso4217;



TRACE **************** **************** **************** ****************;
TRACE *** Manuell: Wechselkurs;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.Manuell_Wechselkurs:
LOAD Distinct
	Key_Wechselkurs,
	Key_Iso4217_Alpha_Datum,
	Key_Iso4217_Alpha,
	Key_Kalender_Datum,
	Wechselkurs_Rate,
	Wechselkurs_ReRate
FROM
[$(vStdTaskQvdTransact)\Einkaufstracker.Wechselkurs.qvd]
(qvd)
Where	Flag_Gueltig=1
and		Match(Key_Iso4217_Alpha,$(vCalcWaehrungRate))
;


FOR Each vManuellWaehrung in $(vCalcWaehrungRate)

	NoConcatenate
	[Realcontrolling.Manuell_Wechselkurs_$(vManuellWaehrung)]:
	LOAD Distinct
		*
	Resident
	Realcontrolling.Manuell_Wechselkurs
	Where	Key_Iso4217_Alpha='$(vManuellWaehrung)'
	;


	Left Join
	([Realcontrolling.Manuell_Wechselkurs_$(vManuellWaehrung)])
	LOAD Distinct
		Date(Manuell_DatumManuellVon,'YYYYMMDD')												as Key_Kalender_Datum,
		Key_Abschlussbericht,
		Key_Iso4217_Alpha																		as tmp_Key_Iso4217_Alpha,
		Manuell_PreisAlt,
		Manuell_PreisAlt_PreisEinheit,
		Manuell_PreisAlt_Original,
		Manuell_PreisNeu,
		Manuell_PreisNeu_PreisEinheit,
		Manuell_PreisNeu_Original,
		Manuell_PreisEinsparung,
		Manuell_PreisEinsparung_PreisEinheit,
		Manuell_PreisEinsparung_Original,
		Manuell_Planung_Einsparung,
		Manuell_Planung_Einsparung_Original,
		Manuell_Planung_EinsparungTag,
		Manuell_Planung_EinsparungTag_Original,
		Manuell_Pauschale_Einsparung,
		Manuell_Pauschale_Einsparung_Original
	Resident
	Realcontrolling.Manuell
	;

	Left Join
	([Realcontrolling.Manuell_Wechselkurs_$(vManuellWaehrung)])
	LOAD Distinct
		If(Flag_TrackingArt='V',Date(Manuell_DatumRealisiert,'YYYYMMDD')
			,Date(Manuell_Pauschale_GueltigRealisiert,'YYYYMMDD'))								as Key_Kalender_Datum,
		Key_Abschlussbericht,
		Key_Iso4217_Alpha																		as tmp_Key_Iso4217_Alpha,
		Manuell_Realisiert_Einsparung,
		Manuell_Realisiert_Einsparung_Original
	Resident
	Realcontrolling.Manuell
	Where	Flag_EinsparungRealisiert='Y'
	and		Flag_TrackingArt='V'
	;


	Left Join
	(Realcontrolling.Manuell)
	LOAD Distinct
		Key_Abschlussbericht,

		// Manuell
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_PreisAlt_Original
			,Wechselkurs_Rate*Manuell_PreisAlt),$(vStdFormatNumberDec))							as [Manuell_PreisAlt_$(vManuellWaehrung)],
		Num(Wechselkurs_Rate*Manuell_PreisAlt_PreisEinheit,$(vStdFormatNumberDec))				as [Manuell_PreisAlt_PreisEinheit_$(vManuellWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_PreisNeu_Original
			,Wechselkurs_Rate*Manuell_PreisNeu),$(vStdFormatNumberDec))							as [Manuell_PreisNeu_$(vManuellWaehrung)],
		Num(Wechselkurs_Rate*Manuell_PreisNeu_PreisEinheit,$(vStdFormatNumberDec))				as [Manuell_PreisNeu_PreisEinheit_$(vManuellWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_PreisEinsparung_Original
			,Wechselkurs_Rate*Manuell_PreisEinsparung),$(vStdFormatNumberDec))					as [Manuell_PreisEinsparung_$(vManuellWaehrung)],
		Num(Wechselkurs_Rate*Manuell_PreisEinsparung_PreisEinheit,$(vStdFormatNumberDec))		as [Manuell_PreisEinsparung_PreisEinheit_$(vManuellWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_Planung_Einsparung_Original
			,Wechselkurs_Rate*Manuell_Planung_Einsparung),$(vStdFormatNumberDec))				as [Manuell_Planung_Einsparung_$(vManuellWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_Planung_EinsparungTag_Original
			,Wechselkurs_Rate*Manuell_Planung_EinsparungTag),$(vStdFormatNumberDec))			as [Manuell_Planung_EinsparungTag_$(vManuellWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_Pauschale_Einsparung_Original
			,Wechselkurs_Rate*Manuell_Pauschale_Einsparung),$(vStdFormatNumberDec))				as [Manuell_Pauschale_Einsparung_$(vManuellWaehrung)],
		Num(If(Key_Iso4217_Alpha=tmp_Key_Iso4217_Alpha,Manuell_Realisiert_Einsparung_Original
			,Wechselkurs_Rate*Manuell_Realisiert_Einsparung),$(vStdFormatNumberDec))			as [Manuell_Realisiert_Einsparung_$(vManuellWaehrung)]
	Resident
	[Realcontrolling.Manuell_Wechselkurs_$(vManuellWaehrung)]
	;


	// Clear
	CALL subCommonDropTable('Realcontrolling.Manuell_Wechselkurs_$(vManuellWaehrung)');

NEXT vManuellWaehrung


// Clear
CALL subCommonDropTable('Realcontrolling.Manuell_Wechselkurs');
LET vManuellWaehrung		= Null();

