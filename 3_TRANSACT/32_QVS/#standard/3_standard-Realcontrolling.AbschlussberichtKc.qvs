// ******** TRANSACT - Realcontrolling.AbschlussberichtKc ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2018-04-05	1.00		created
// 2015-05-15	1.10		mod: Einsparung
//
// ******** TRANSACT - Realcontrolling.AbschlussberichtKc ********



TRACE **************** **************** **************** ****************;
TRACE *** AbschlussberichtKc: Prepare;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.AbschlussberichtKc_tmp:
LOAD
	*
FROM
	[$(vStdTaskQvdTransact)\Realcontrolling.Abschlussbericht.qvd]
	(qvd)
;



TRACE **************** **************** **************** ****************;
TRACE *** AbschlussberichtKc: Einsparung;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.AbschlussberichtKc_Realisiert:
LOAD Distinct
	Abschlussbericht_Name
Resident
	Realcontrolling.AbschlussberichtKc_tmp
;


// **************** ****************
// *** Manuell
// **************** ****************

IF FileSize('$(vStdTaskQvdTransact)\Realcontrolling.Manuell.qvd')>0 then

	Left Join
	(Realcontrolling.AbschlussberichtKc_Realisiert)
	LOAD
		Abschlussbericht_Name,
		Sum(Einsparung_Realisiert_Einsparung)												as Einsparung_Realisiert_Manuell
	FROM
		[$(vStdTaskQvdTransact)\Realcontrolling.Einsparung.qvd]
		(qvd)
	Group By
		Abschlussbericht_Name
	;

ELSE

	Left Join
	(Realcontrolling.AbschlussberichtKc_Realisiert)
	LOAD * Inline [
		Einsparung_Realisiert_Manuell
	];

ENDIF


// **************** ****************
// *** EinsparungBestellung
// **************** ****************

IF FileSize('$(vStdTaskQvdTransact)\Realcontrolling.EinsparungBestellung.qvd')>0 then

	Left Join
	(Realcontrolling.AbschlussberichtKc_Realisiert)
	LOAD
		Abschlussbericht_Name,
		Sum(Einsparung_Realisiert_Einsparung)												as Einsparung_Realisiert_Bestellung
	FROM
		[$(vStdTaskQvdTransact)\Realcontrolling.EinsparungBestellung.qvd]
		(qvd)
	Group By
		Abschlussbericht_Name
	;

ELSE

	Left Join
	(Realcontrolling.AbschlussberichtKc_Realisiert)
	LOAD * Inline [
		Einsparung_Realisiert_Bestellung
	];

ENDIF


// **************** ****************
// *** EinsparungKreditor
// **************** ****************

IF FileSize('$(vStdTaskQvdTransact)\Realcontrolling.EinsparungKreditor.qvd')>0 then

	Left Join
	(Realcontrolling.AbschlussberichtKc_Realisiert)
	LOAD
		Abschlussbericht_Name,
		Sum(Einsparung_Realisiert_Einsparung)												as Einsparung_Realisiert_Kreditor
	FROM
		[$(vStdTaskQvdTransact)\Realcontrolling.EinsparungKreditor.qvd]
		(qvd)
	Group By
		Abschlussbericht_Name
	;

ELSE

	Left Join
	(Realcontrolling.AbschlussberichtKc_Realisiert)
	LOAD * Inline [
		Einsparung_Realisiert_Kreditor
	];

ENDIF


// **************** ****************
// *** Final
// **************** ****************

Left Join
(Realcontrolling.AbschlussberichtKc_tmp)
LOAD
	Abschlussbericht_Name,
	If(Abschlussbericht_Total_Einsparung>=0,'Y','N')										as Flag_AbschlussberichtRealisiert,
	Abschlussbericht_Total_Einsparung,
	Abschlussbericht_Total_Manuell,
	Abschlussbericht_Total_Bestellung,
	Abschlussbericht_Total_Kreditor
;
LOAD
	*,
	Abschlussbericht_Total_Manuell
		+ Abschlussbericht_Total_Bestellung
		+ Abschlussbericht_Total_Kreditor													as Abschlussbericht_Total_Einsparung
;
LOAD Distinct
	Abschlussbericht_Name,
	If(IsNum(Einsparung_Realisiert_Manuell),Num(Einsparung_Realisiert_Manuell),0)			as Abschlussbericht_Total_Manuell,
	If(IsNum(Einsparung_Realisiert_Bestellung),Num(Einsparung_Realisiert_Bestellung),0)		as Abschlussbericht_Total_Bestellung,
	If(IsNum(Einsparung_Realisiert_Kreditor),Num(Einsparung_Realisiert_Kreditor),0)			as Abschlussbericht_Total_Kreditor
Resident
	Realcontrolling.AbschlussberichtKc_Realisiert
;


// Clear
CALL subCommonDropTable('Realcontrolling.AbschlussberichtKc_Realisiert');



TRACE **************** **************** **************** ****************;
TRACE *** Abschlussbericht: Final;
TRACE **************** **************** **************** ****************;

NoConcatenate
Realcontrolling.AbschlussberichtKc:
LOAD Distinct
	*
Resident
	Realcontrolling.AbschlussberichtKc_tmp
;


// Clear
CALL subCommonDropTable('Realcontrolling.AbschlussberichtKc_tmp');
