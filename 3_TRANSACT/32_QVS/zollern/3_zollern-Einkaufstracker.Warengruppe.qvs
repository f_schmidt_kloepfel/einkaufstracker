// ******** TRANSACT - Einkaufstracker.Warengruppe ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2017-06-12	1.00		created
// 2017-09-20	2.00		mod: Template.xlsx; add: HierarchyBelongsTo
// 2017-09-21	2.01		modified; rem: HierarchyBelongsTo
// 2017-09-22	2.02		modified; add: HierarchyBelongsTo
// 2017-10-15	2.10		add: Wochenbericht
// 2017-10-23	3.00		modified
// 2017-11-19	4.00		modified
// 2017-12-14	4.10		add Field: Flag_UnbekanntAlias
// 2018-01-26	5.10		add Fields: Key_Warengruppe_Name*_KC, Warengruppe_Id_KC; mod Field: Flag_Warengruppe_KC
// 2018-02-09	5.11		mod: KC Mapping
// 2018-02-21	5.12		add Field: Flag_Scope
//
// ******** TRANSACT - Einkaufstracker.Warengruppe ********



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: Prepare;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.Warengruppe_tmp:
LOAD Distinct
	Key_Warengruppe,
	Key_Warengruppe								as Key_Warengruppe_Code,
	Key_Warengruppe								as Key_Warengruppe_Name,
	0											as Flag_Warengruppe,
	0											as Flag_WarengruppeCode,
	Flag_Unbekannt,
	Null()										as Warengruppe_Code,
	Warengruppe_Warengruppe1,
	Null()										as Warengruppe_Warengruppe2,
	Null()										as Warengruppe_Warengruppe3
;
LOAD * Inline [
	Key_Warengruppe,					Warengruppe_Warengruppe1,	Flag_Unbekannt
	'$(vStdIniDescriptionUnknownId)',	'unbekannte WG',			2
	'$(vStdIniDescriptionDuplicateId)',	'mehrdeutige WG',			3
];



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: WarengruppeStamm;
TRACE **************** **************** **************** ****************;

IF FileSize('$(vStdTaskQvdTransact)\Einkaufstracker.WarengruppeStamm.qvd')>0 then

	Concatenate
	(Einkaufstracker.Warengruppe_tmp)
	LOAD Distinct
		*,
		Lower(Key_Warengruppe2)					as join_Key_Warengruppe2,
		0										as Flag_Unbekannt
	FROM
	[$(vStdTaskQvdTransact)\Einkaufstracker.WarengruppeStamm.qvd]
	(qvd)
	;

ELSE

	Left Join
	(Einkaufstracker.Warengruppe_tmp)
	LOAD * Inline [
		WerkGesellschaft_Gesellschaft, WerkGesellschaft_WerkStandort, WerkGesellschaft_WerkNr, WerkGesellschaft_WerkName, WerkGesellschaft_LagerNr, WerkGesellschaft_LagerName
	];

	Left Join
	(Einkaufstracker.Warengruppe_tmp)
	LOAD * Inline [
		Warengruppe_Indirekt, Warengruppe_PersonManager, Warengruppe_PersonEinkaufsleiter, Warengruppe_ZHG, Warengruppe_ZHGName
	];

ENDIF



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: Artikel;
TRACE **************** **************** **************** ****************;

IF FileSize('$(vStdTaskQvdTransact)\Einkaufstracker.ArtikelStamm.qvd')>0 then

	Concatenate
	(Einkaufstracker.Warengruppe_tmp)
	LOAD Distinct

		// Keys
		Key_Warengruppe,
		Key_Warengruppe_Code,
		Text(If(Index(Key_Warengruppe,'#'),Mid(Key_Warengruppe,Index(Key_Warengruppe,'#',-1)+1)
			,Key_Warengruppe))					as Key_Warengruppe_Name,
		'###'									as Key_WerkGesellschaft,
		'#####'									as Key_WerkGesellschaft_Lager,
		'####'& Key_Warengruppe					as Key_WerkGesellschaft_Warengruppe,
		'######'& Key_Warengruppe				as Key_WerkGesellschaft_Lager_Warengruppe,

		// Flag
		0										as Flag_WerkGesellschaft,
		Flag_Warengruppe,
		Flag_WarengruppeCode,
		0										as Flag_Unbekannt,

		// Warengruppe
		Warengruppe_WarengruppeCode,
		Warengruppe_WarengruppeCode				as Warengruppe_Warengruppe1

	FROM
	[$(vStdTaskQvdTransact)\Einkaufstracker.ArtikelStamm.qvd]
	(qvd)
	Where	Flag_Warengruppe=1
	and		not Exists(Key_Warengruppe)
	and		not Exists(Key_WerkGesellschaft_Warengruppe)
	and		not Exists(Key_WerkGesellschaft_Warengruppe,'####'& Key_Warengruppe)
	;

ENDIF



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: Bestellung/Rechnung/KreditorUmsatz;
TRACE **************** **************** **************** ****************;

FOR Each vWarengruppeQuelle in 'Artikel','Bestellung','Rechnung','KreditorUmsatz'
	IF FileSize('$(vStdTaskQvdTransact)\Einkaufstracker.$(vWarengruppeQuelle).qvd')>0 then

		Concatenate
		(Einkaufstracker.Warengruppe_tmp)
		LOAD Distinct

			// Keys
			Key_Warengruppe,
			Key_Warengruppe_Code,
			Text(If(Index(Key_Warengruppe,'#'),Mid(Key_Warengruppe,Index(Key_Warengruppe,'#',-1)+1)
				,Key_Warengruppe))				as Key_Warengruppe_Name,

			'###'								as Key_WerkGesellschaft,
			'#####'								as Key_WerkGesellschaft_Lager,
			'####'& Key_Warengruppe				as Key_WerkGesellschaft_Warengruppe,
			'######'& Key_Warengruppe			as Key_WerkGesellschaft_Lager_Warengruppe,

			// Flag
			0									as Flag_WerkGesellschaft,
			Flag_Warengruppe,
			Flag_WarengruppeCode,
			1									as Flag_Unbekannt,

			// Warengruppe
			Warengruppe_WarengruppeCode,
			Warengruppe_WarengruppeCode			as Warengruppe_Warengruppe1

		FROM
		[$(vStdTaskQvdTransact)\Einkaufstracker.$(vWarengruppeQuelle).qvd]
		(qvd)
		Where	Flag_Warengruppe=1
		and		not Exists(Key_Warengruppe)
		and		not Exists(Key_WerkGesellschaft_Warengruppe)
		and		not Exists(Key_WerkGesellschaft_Warengruppe,'####'& Key_Warengruppe)
		;

	ENDIF
NEXT vWarengruppeQuelle


// Clear
LET vWarengruppeQuelle		= Null();



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: Wochenbericht;
TRACE **************** **************** **************** ****************;

IF FileSize('$(vStdTaskQvdTransact)\KC.Wochenbericht.qvd')>0 then

	Concatenate
	(Einkaufstracker.Warengruppe_tmp)
	LOAD Distinct

		// Keys
		Key_Warengruppe,
		Key_Warengruppe_Code,
		Text(Key_Warengruppe_Name)			as Key_Warengruppe_Name,
		'###'								as Key_WerkGesellschaft,
		'###'& Key_Warengruppe				as Key_WerkGesellschaft_Warengruppe,

		// Flag
		1									as Flag_Warengruppe,
		0									as Flag_WarengruppeCode,
		0									as Flag_Unbekannt,

		// Warengruppe
		Warengruppe_WarengruppeCode,
		Warengruppe_Warengruppe1,
		Warengruppe_Warengruppe2

	FROM
	[$(vStdTaskQvdTransact)\KC.Wochenbericht.qvd]
	(qvd)
	;

ENDIF



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: Hierarchie;
TRACE **************** **************** **************** ****************;


// **************** ****************
// *** Level 1
// **************** ****************

NoConcatenate
Einkaufstracker.Warengruppe_hier:
LOAD Distinct

	// Keys
	Key_Warengruppe,
	Key_Warengruppe_Code,
	Key_Warengruppe_Name,
	Key_WerkGesellschaft,
	Key_WerkGesellschaft_Lager,
	Key_WerkGesellschaft_Warengruppe,
	Key_WerkGesellschaft_Lager_Warengruppe,

	// Hierarchie
	Warengruppe_WarengruppeCode,
	Warengruppe_Warengruppe1						as Warengruppe_Pfad,
	Warengruppe_Warengruppe1						as Warengruppe_PfadParent,
	Warengruppe_Warengruppe1						as Warengruppe_Name,
	1												as Warengruppe_Ebene,
	Warengruppe_Warengruppe1,
	Warengruppe_Warengruppe2,
	Warengruppe_Warengruppe3

Resident
Einkaufstracker.Warengruppe_tmp
;


// **************** ****************
// *** Level 2
// **************** ****************

Concatenate
(Einkaufstracker.Warengruppe_hier)
LOAD Distinct

	// Keys
	Key_Warengruppe,
	Key_Warengruppe_Code,
	Key_Warengruppe_Name,
	Key_WerkGesellschaft,
	Key_WerkGesellschaft_Lager,
	Key_WerkGesellschaft_Warengruppe,
	Key_WerkGesellschaft_Lager_Warengruppe,

	// Hierarchie
	Warengruppe_WarengruppeCode,
	Warengruppe_Warengruppe1
		&'#'& Warengruppe_Warengruppe2				as Warengruppe_Pfad,
	Warengruppe_Warengruppe1						as Warengruppe_PfadParent,
	Warengruppe_Warengruppe2						as Warengruppe_Name,
	2												as Warengruppe_Ebene,
	Warengruppe_Warengruppe1,
	Warengruppe_Warengruppe2,
	Warengruppe_Warengruppe3

Resident
Einkaufstracker.Warengruppe_tmp
Where	Len(Trim(Warengruppe_Warengruppe2))>0
;


// **************** ****************
// *** Level 3
// **************** ****************

Concatenate
(Einkaufstracker.Warengruppe_hier)
LOAD Distinct

	// Keys
	Key_Warengruppe,
	Key_Warengruppe_Code,
	Key_Warengruppe_Name,
	Key_WerkGesellschaft,
	Key_WerkGesellschaft_Lager,
	Key_WerkGesellschaft_Warengruppe,
	Key_WerkGesellschaft_Lager_Warengruppe,

	// Hierarchie
	Warengruppe_WarengruppeCode,
	Warengruppe_Warengruppe1
		&'#'& Warengruppe_Warengruppe2
		&'#'& Warengruppe_Warengruppe3				as Warengruppe_Pfad,
	Warengruppe_Warengruppe1
		&'#'& Warengruppe_Warengruppe2				as Warengruppe_PfadParent,
	Warengruppe_Warengruppe3						as Warengruppe_Name,
	3												as Warengruppe_Ebene,
	Warengruppe_Warengruppe1,
	Warengruppe_Warengruppe2,
	Warengruppe_Warengruppe3

Resident
Einkaufstracker.Warengruppe_tmp
Where	Len(Trim(Warengruppe_Warengruppe3))>0
;



// **************** ****************
// *** Fields
// **************** ****************

Left Join
(Einkaufstracker.Warengruppe_hier)
LOAD Distinct

	// Keys
	Key_Warengruppe,

	// Flag
	Lower(Trim(Key_Warengruppe_Name))				as join_Key_Warengruppe,
//	Lower(If(SubStringCount(Key_Warengruppe_Name,'#')>2,Mid(Key_Warengruppe_Name,Index(Key_Warengruppe_Name,'#',2)-1)
//		,Key_Warengruppe_Name))						as wochenbericht_Key_Warengruppe,
	Flag_Warengruppe,
	Flag_WarengruppeCode,
	Flag_Unbekannt,
	If(Len(Trim(Warengruppe_Warengruppe2))>0,1,0)	as Flag_Warengruppe2,
	If(Len(Trim(Warengruppe_Warengruppe3))>0,1,0)	as Flag_Warengruppe3,

	// Warengruppe
	If(Match(Upper(Left(Warengruppe_Indirekt,1)),0,'N'),'N'
		,If(Match(Upper(Left(Warengruppe_Indirekt,1)),1,'J','Y'),'J'
		,'U'))										as Warengruppe_Indirekt,

	Warengruppe_PersonManager,
	Warengruppe_PersonEinkaufsleiter,
	Warengruppe_ZHG,
	Warengruppe_ZHGName

Resident
Einkaufstracker.Warengruppe_tmp
;


Left Join
(Einkaufstracker.Warengruppe_hier)
LOAD Distinct
	Key_WerkGesellschaft_Lager,
	Flag_WerkGesellschaft,
	WerkGesellschaft_Gesellschaft,
	WerkGesellschaft_WerkStandort,
	WerkGesellschaft_WerkNr,
	WerkGesellschaft_WerkName,
	WerkGesellschaft_LagerNr,
	WerkGesellschaft_LagerName
Resident
Einkaufstracker.Warengruppe_tmp
;


// Clear
CALL subCommonDropTable('Einkaufstracker.Warengruppe_tmp');



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: KC Mapping;
TRACE **************** **************** **************** ****************;

IF FileSize('$(vStdTaskQvdTransact)\KC.Warengruppe.qvd')>0 then

	Left Join
	(Einkaufstracker.Warengruppe_hier)
	LOAD Distinct
		Lower(Trim(Key_Warengruppe_Name))			as join_Key_Warengruppe,
		Key_Warengruppe_Name_KC						as tmp_Key_Warengruppe_Name_KC,
		1											as tmp_Flag_Warengruppe_KC,
		Flag_Warengruppe2_KC						as tmp_Flag_Warengruppe2_KC,
		Flag_Warengruppe3_KC						as tmp_Flag_Warengruppe3_KC,
		Warengruppe_Warengruppe1_KC					as tmp_Warengruppe_Warengruppe1_KC,
		Warengruppe_Warengruppe2_KC					as tmp_Warengruppe_Warengruppe2_KC,
		Warengruppe_Warengruppe3_KC					as tmp_Warengruppe_Warengruppe3_KC
	FROM
	[$(vStdTaskQvdTransact)\KC.Warengruppe.qvd]
	(qvd)
	;

ELSE

	Left Join
	(Einkaufstracker.Warengruppe_hier)
	LOAD * Inline [
		tmp_Key_Warengruppe_Name_KC, tmp_Flag_Warengruppe_KC, tmp_Flag_Warengruppe2_KC, tmp_Flag_Warengruppe3_KC, tmp_Warengruppe_Warengruppe1_KC, tmp_Warengruppe_Warengruppe2_KC, tmp_Warengruppe_Warengruppe3_KC
	];

ENDIF


// Clear
//DROP Field join_Key_Warengruppe;



TRACE **************** **************** **************** ****************;
TRACE *** Warengruppe: Final;
TRACE **************** **************** **************** ****************;

NoConcatenate
Einkaufstracker.Warengruppe:
LOAD Distinct
	*,

	// Keys
	Warengruppe_Warengruppe1_KC																	as Key_Warengruppe1_KC,
	If(Flag_Warengruppe2_KC=1,Warengruppe_Warengruppe1_KC
		&'#'& Warengruppe_Warengruppe2_KC)														as Key_Warengruppe2_KC,
	If(Flag_Warengruppe3_KC=1,Key_Warengruppe_Name_KC)											as Key_Warengruppe3_KC,

	// Fields
	AutoNumber(Key_Warengruppe_Name_KC,12)														as Warengruppe_Id_KC

;
LOAD
	*,

	// Keys
	Warengruppe_Warengruppe1																	as Key_Warengruppe1,
	If(Flag_Warengruppe2=1,Warengruppe_Warengruppe1 &'#'& Warengruppe_Warengruppe2)				as Key_Warengruppe2,
	If(Flag_Warengruppe3=1,Key_Warengruppe_Name)												as Key_Warengruppe3,
	If(Len(Trim(tmp_Key_Warengruppe_Name_KC))>0,tmp_Key_Warengruppe_Name_KC
		,Key_Warengruppe_Name)																	as Key_Warengruppe_Name_KC,

	// Flag
	If(Lower(Trim(tmp_Warengruppe_Warengruppe1_KC))='out of scope','N','Y')						as Flag_Scope,
	If(Flag_Unbekannt=3,'mehrdeutig',If(Match(Flag_Unbekannt,1,2),'unbekannt','bekannt'))		as Flag_UnbekanntAlias,
	If(tmp_Flag_Warengruppe_KC=1,1,0)															as Flag_Warengruppe_KC,
	If(tmp_Flag_Warengruppe2_KC>0,tmp_Flag_Warengruppe2_KC,0)									as Flag_Warengruppe2_KC,
	If(tmp_Flag_Warengruppe3_KC>0,tmp_Flag_Warengruppe3_KC,0)									as Flag_Warengruppe3_KC,

	// Warengruppe
	AutoNumber(Key_Warengruppe,12)																as Warengruppe_Id,
	If(Warengruppe_Indirekt='J','indirekt'
		,If(Warengruppe_Indirekt='N','direkt'
		,'unbekannt'))																			as Warengruppe_IndirektAlias,

	// KC
	If(tmp_Flag_Warengruppe_KC=1,tmp_Warengruppe_Warengruppe1_KC,Warengruppe_Warengruppe1)		as Warengruppe_Warengruppe1_KC,
	If(tmp_Flag_Warengruppe2_KC=1,tmp_Warengruppe_Warengruppe2_KC,Warengruppe_Warengruppe2)		as Warengruppe_Warengruppe2_KC,
	If(tmp_Flag_Warengruppe3_KC=1,tmp_Warengruppe_Warengruppe3_KC,Warengruppe_Warengruppe3)		as Warengruppe_Warengruppe3_KC,

	// Bezeichnung
	If(not Match(Key_Warengruppe,'$(vStdIniDescriptionUnknownId)','$(vStdIniDescriptionDuplicateId)'),'OWG: ')
		& Warengruppe_Warengruppe1																as Warengruppe_Warengruppe1Bezeichnung,
	If(Flag_Warengruppe2=1,'WG: '& Warengruppe_Warengruppe2)									as Warengruppe_Warengruppe2Bezeichnung,
	If(Flag_Warengruppe3=1,'UWG: '& Warengruppe_Warengruppe3)									as Warengruppe_Warengruppe3Bezeichnung

Resident
Einkaufstracker.Warengruppe_hier
;


// **************** ****************
// *** Clear
// **************** ****************

CALL subCommonDropTable('Einkaufstracker.Warengruppe_hier');

DROP Fields
	tmp_Key_Warengruppe_Name_KC
	,tmp_Flag_Warengruppe_KC
	,tmp_Flag_Warengruppe2_KC
	,tmp_Flag_Warengruppe3_KC
	,tmp_Warengruppe_Warengruppe1_KC
	,tmp_Warengruppe_Warengruppe2_KC
	,tmp_Warengruppe_Warengruppe3_KC
;
